#How to use:

## 1) Run MATLAB (tested with R2019a)
## 2) Change to the folder with the code
## 3) Add the folder to path
```
addpath(genpath(pwd));
```
## 4) Compile the C code
```
system("gcc src/c/run_hmm_console.c -o src/c/test/hmm_console.out -lm");
```
## 5) To generate the figures in the main text, run
### Fig1:
```
example_hmmsv_fig1
```
### Fig3:
```
example_hmmsv_fig3
```
### Fig4:
```
example_hmmsv_fig4
```
### Fig5:
```
example_hmmsv_fig5
```
### Fig6:
```
example_hmmsv_fig6
```