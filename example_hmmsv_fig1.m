settings = 'schematic_example.txt';

sets = ini2struct(settings);
% % chech if the sequences have to be reproducible
if sets.reproducible
    rng(1,'twister');
end

%% test which is plotted in paper
% len1 = 500;
% lenVar = 100;
sets.outfold = outfold;

sets.svList = [1 6 2 3 4 ];

% generate sample circular structural variations
import Rand.linear_sv;
[bar1, bar2,matchTable] = arrayfun(@(x) linear_sv(sets.length, sets.lengthVar, x, sets.sigma,sets.circ),sets.svType ,'un',false);


% create random /todo: move to linear_sv
sets.len1 = sets.length;
sets.len2 = sets.lengthVar;
sets.typeSv = [2 4];
sets.noise = 0;
[bar1D,bar2D,matchT,matchPos] = generate_random(sets);

bar1{end+1} =bar1D{1};
bar2{end+1} = bar2D{end};
matchTable{end+1} = matchT;



sets.svType = { '(A) Insertion', '(B) Deletion', '(C) Inversion', '(D) Repeat','(E) Translocation', '(F) Inversion+Translocation '};
     
% maybe change to stackedplots in the future?
% sets.outfold = ;
% Fig1-eps-converted-to.pdf
import Plot.plot_sv;
[~,f] = plot_sv( bar1, bar2,matchTable,sets,'Fig1.eps');
 print(f, '-dtiff', fullfile(sets.outfold ,'/Fig1.tif'),'-r300');
