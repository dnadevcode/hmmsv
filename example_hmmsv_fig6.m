%
%% compare blast
seq = 'test6/sequence-2.fasta';
system(strcat(['makeblastdb -in ' seq ' -dbtype nucl']));
% seq2 = '/home/albyback/git/hmmsv/test/data/DA15001-pUUH_Sequence.fasta';
seq2 = 'test6/puuh2392.fasta';

out = 'comp.txt';
system(strcat(['blastn -query ' seq2 ' -db ' seq ' -perc_identity 90 -outfmt 6  -out ' out]));
data = importdata(out)
trueTable = data.data(1:3,5:8);

% create cb barcode
bpnm = 0.2611;   % here could get the lambda value from kymographs..
pixelWidth_nm = 159.2;

import CB.create_cb_barcode;
[tQ,matFilepathShortQ,theoryStructQ,theoryGenQ]  = create_cb_barcode(seq2,bpnm,pixelWidth_nm);

[t,matFilepathShort,theoryStruct,theoryGen]  = create_cb_barcode(seq,bpnm,pixelWidth_nm);

fd= fopen(matFilepathShort,'r');
file = fscanf(fd,'%s');
fclose(fd);

% Example how to run consensus vs consensus for hmmsv. Single query
% and single data
%%
query = repmat({'test/puuh_coonsensus_159.2_0.26111_.mat'},1,1);
% two theory files //generated with HCA
data = {file};


% data = repmat({'/home/albyback/git/sv/data/runData80kb/sample1/P6K0.mat'},1,1);
% query = {'/home/albyback/git/sv/data/runData80kb/sample1/P6K25.mat'};
settingsHMM = ini2struct( 'hmmsv_default_settings_anc.txt');

settingsHMM.query.type = 'consensus';
settingsHMM.query.dataFile = 'src/settings/query.txt';

settingsHMM.data.type = 'theory';
settingsHMM.data.dataFile = 'src/settings/data.txt';

import Import.create_txt_file;
create_txt_file(settingsHMM.query,query);
create_txt_file(settingsHMM.data,data);
settingsHMM.name = 'puuhanc';

settingsHMM.pval.pvalFile = [];

hmmsv_run(settingsHMM)

%% 
numFragments = 12;
data = importdata('comp.txt')
trueTable = data.data(1:numFragments,5:8);
% prepare data for the HMM analysis, these are queries.
timestamp = datestr(clock(), 'yyyy-mm-dd_HH_MM_SS');
settingsHMM.timestamp = timestamp;

import Import.get_user_settings;
settingsHMM = get_user_settings(settingsHMM);


restest = res;
shift=-29; %hardcoded, so that main part of barcodes would be aligned

for j=1:length(restest)
    import functions.create_full_table;
    [res_table,barfragq,barfragr] = create_full_table(res{j}.matchTable,res{j}.bar1,res{j}.bar2',1);
    [s,pos] = max(cellfun(@(x) length(x),barfragq));
%     shift = -(res_table(pos,1)-min(res_table(pos,3),res_table(pos,3)-length(res{j}.bar2)));
%     restest{j}.bar1  = circshift( restest{j}.bar1, [0,-shift]);
%     for k=1:size(restest{j}.matchTable,1)
%         restest{j}.matchTable(k,1:2) = mod( restest{j}.matchTable(k,1:2)-1-shift,length( restest{j}.bar1))+1; 
%     end
%         shift = -(res_table(pos,1)-min(res_table(pos,3),res_table(pos,3)-length(res{j}.bar2)));
    restest{j}.bar2  = circshift( restest{j}.bar2, [0,-shift]);
    for k=1:size(restest{j}.matchTable,1)
        restest{j}.matchTable(k,3:4) = mod( restest{j}.matchTable(k,3:4)-1-shift,length( restest{j}.bar2))+1; 
    end

end



import Plot.plot_sv_full_vs;
% f=figure;hold on
% subplot(2,2,1)
bppx = 611;     
f=plot_sv_full_vs( restest,settingsHMM,strcat(num2str(1),settingsHMM.name),2,0,bppx/1000);
 ylabel('Shifted barcode intensities','FontSize', 10,'Interpreter','latex');

ax=  subplot(2,2,[3]);

[resultStruct,~] = create_table_thr(trueTable,theoryGen,theoryGenQ,numFragments,bppx);


bpPerPx = bppx/1000;
ColOrd = get(ax, 'ColorOrder');
bar1 = zscore(resultStruct.bar1);
bar2 = zscore(resultStruct.bar2);
res_table = resultStruct.matchTable;


ylmax = max(bar1);
ylmin = min(bar1);
yumax = max(bar2+16);
yumin = min(bar2+16);  
hold on;
% plot a box
pX = [1 length(bar2) length(bar2) 1];
pY = [ylmin  ylmin ylmax ylmax];
% h(1) = plot(ax, bar2, 'Color', 'black');
patch(pX, pY, 'black', 'faceAlpha', 0.1, ...
      'edgeAlpha', 0.3, 'edgeColor', 'black');
hold on;
pX = [1 length(bar1) length(bar1) 1];
pY = [yumin  yumin yumax yumax];
patch(pX, pY, 'black', 'faceAlpha', 0.1, ...
      'edgeAlpha', 0.3, 'edgeColor', 'black');
text( length(bar1),(yumax+yumin)/2,'pKPN3')
text( length(bar2),(ylmax+ylmin)/2,'pUUH239.2')

  
% h(2) = plot(ax, bar1+16, 'Color', 'black');

% number of fragments. TODO: keep an option of having different

% fragments appear as a single fragment in output?
numFragments = max(res_table(:,6));

% convert to full table:
%     res_table
import functions.create_full_table;
res_table = create_full_table(res_table,resultStruct.bar1,resultStruct.bar2);
legendNames = cell(1,numFragments);
%     end
fullTable = [];
N = length(bar1);    M = length(bar2);

col = 'black';
% ColOrd(1+mod(res_table(i,6), 7), :)
% problem: both bar1 and bar2 can be circularly shifted..
for i = 1:size(res_table, 1)
    pX = res_table(i, [1 1 2 2 4 4 3 3]);
    pX = pX + [-0.5 -0.5 0.5 0.5 -0.5 -0.5 0.5 0.5];
    pY = [yumin yumax yumax yumin ylmax ylmin ylmin ylmax];
    pY = pY + [0 0.5 0.5 0 0 -0.5 -0.5 0 ];
    if resultStruct.pass(res_table(i,6)) == 1
        h(i+2) = patch(pX, pY, col, 'faceAlpha', 0.1, ...
              'edgeAlpha', 0.3, 'edgeColor', col);
    else
        if ~plotonlypass
           % this does not pass the thresh 
            h(i+2) = patch(pX, pY,col, 'faceAlpha',0, ...
                  'edgeAlpha', 0.5, 'edgeColor',col,'LineStyle','--');
        end
        % uint8([17 17 17])
    end
end

hold off;

clusters = find(~cellfun(@isempty,legendNames));


ticks = 1:50/bpPerPx:2*length(bar2);
ticksx = floor(ticks*bpPerPx);
ax.XTick = [ticks];
ax.XTickLabel = [ticksx];   
ax.YTick =[];
% ax.Ytick = [];
xlabel( ax,'Position (kb)','FontSize', 10,'Interpreter','latex')
outfold = 'output/';

 print(f, '-dtiff', fullfile(outfold,'/Fig6.tif'),'-r300');
saveas(f,fullfile(outfold,'/Fig6.eps'),'epsc')

        saveas(f,fullfile(outfold,'.ansecstorplot.jpg'))

