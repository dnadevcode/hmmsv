%
% 1) change to the folder with example
cd(fileparts(which('example_hmmsv_fig3.m')));

% 2) generate path
addpath(genpath(pwd));

% 3) create output folder
mkdir("output");
outfold = 'output/';
% 4) compile c code
system("gcc src/c/run_hmm_console.c -o src/c/test/hmm_console.out -lm")

% pre-generated p-value file / saves time, alternatively generate
pval1 = 'test/pregenerated_500_500_44_500_1.8844_1_pvals.mat';
pval2 = 'test/pregenerated_500_550_44_500_1.8844_1_pvals.mat';
% 
% import Pvalue.pregenerate_values;
% sets.pval.pvalFile = pregenerate_values(500, 500, 44:500, 1, 1.8844,20,'test3');

        

[~,~,~,res1,settingsHMM] = single_variation_figure(pval2,1);
[res2,settingsHMM2] = double_variation_figure(pval2,1,2,4);


bppx = 0.5; % basepair per pixel ratio
f = figure('Position', [10 10 700 650])

% ax = subplot(1, 4,[1 2 3 4]);
ax = subplot(2,1,1)
import Plot.plot_sv_fullsim;
plot_sv_fullsim( res1{1},settingsHMM,strcat(num2str(1),settingsHMM.name),2,0,bppx);
hold on
ylabel('Shifted barcode intensities','FontSize', 10,'Interpreter','latex');
ax = subplot(2,1,2)

plot_sv_fullsim( res2{1},settingsHMM2,strcat(num2str(1),settingsHMM2.name),2,0,bppx);
ylabel('Shifted barcode intensities','FontSize', 10,'Interpreter','latex');

saveas(f,fullfile(settingsHMM.output.figsfold,'Fig3.eps'),'epsc')
% fullfile(settingsHMM.output.figsfold,strcat(settingsHMM.timestamp,settingsHMM.name))

 print(f, '-dtiff', fullfile(settingsHMM.output.figsfold ,'/Fig3.tif'),'-r300');
