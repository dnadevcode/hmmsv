function [pccVal] = pval_pregenerated(pcc, lenmatch, pvalFile,moreaccurate)
    % compute pval_pregenerated using pregenerateddatabase


    % note here that we don't check kernelsigma validity that should
    % be clear from when inputing the file
    load(pvalFile, 'par1', 'par2', 'wRange');
    

    if nargin <4
        p = @(x,y,z) 1-(0.5+0.5*(1-betainc((x).^2,0.5,y/2-1,'upper'))).^z ;
    else
        p = @(x,y,z) 1-(0.5+vpa(0.5)*(vpa(1,16)-vpa(betainc(x.^2,0.5,y/2-1,'upper'),16))).^z;
    end

    pccVal = ones(1,length(lenmatch));
    for i = 1:length(lenmatch)
        idx =  find(wRange == (min(lenmatch(i),max(wRange)))); % in case we go out of bounds, just take the max wrange value there is
        try
            pccVal(i) = p(pcc(i),par1{idx},par2{idx});
        catch
            pccVal(i) = nan;
        end
    end
    % generate the params here
    
%     pccVal = zeros(1,length(lenmatch));
%     try
%         % alternatively  this is generated using pval_par_full(300:50:600,300:50:600,100,[0,0],1.8844)
%         % or in lunarc j = c.batch(@pval_par_full,1, {300:50:600,300:50:600,1000,[0,0],1.88,'blaSmall'},'Pool',100)
%         par1 = cellfun(@(x) x(1),fullmat);
%         par2 = cellfun(@(x) x(2),fullmat);
%         par1Fun = @(x,y,z) interp3(length1, length2,w, par1, x,y,z,'nearest');
%         par2Fun = @(x,y,z) interp3(length1, length2,w, par2, x,y,z,'nearest');
% %         p = @(x,par1,par2) 1-(0.5+0.5*(1-betainc((x).^2,0.5,par1/2-1,'upper'))).^par2;
% 
%         
%         for i=1:length(pcc)
%             pccVal(i) =  p(pcc(i),par1Fun(len1,len2,lenmatch(i)),par2Fun(len1,len2,lenmatch(i)));
%         end
%     %         pc

    
end

