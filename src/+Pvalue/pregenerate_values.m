function [file] = pregenerate_values(len1,len2, wRange, circ, kernelsigma,numBars,outputfold)
    % Pregenerates p-value statistics ofr range of w values
    
    %   Args:
    %       totalLen - product of len1*len2 
    %       wRange - range of w values (possible lengths)

    % maybe set len1 to max wrange?
    %     len1 = max(wRange); 
    %     len2 = round(totalLen/len1);
    
%     totalLen = len1*len2; % total length -currently unused.., but relevant when 
    
    import Core.run_pval_matlab; % we only compute this for best stretch,
%     [par1, par2, ccMax] = arrayfun(@(x) ...
%         run_pval_matlab(len1, len2, x, circ, kernelsigma,numBars),wRange, 'un',false);
    
    par1= cell(1,length(wRange));
    par2= cell(1,length(wRange));
    ccMax= cell(1,length(wRange));
    thresh = cell(1,length(wRange));
    for i=1:length(wRange)
%         i
        if wRange(i) > 20 % only for width larger than some minima..
            [par1{i}, par2{i}, ccMax{i},thresh{i}] = run_pval_matlab(len1, len2, min([wRange(i) len1 len2]), circ, kernelsigma,numBars); 
        end
    end
    % 
    file = fullfile(outputfold,strcat(['pregenerated_' num2str(len1) '_' num2str(len2) '_' num2str(wRange(1)) '_' num2str(wRange(end)) '_' num2str(kernelsigma)  '_' num2str(circ) '_pvals.mat']));
    % and save these
    save(file,'par1', 'par2', 'ccMax','thresh','len1','len2','wRange','circ','kernelsigma','numBars','-v7.3');

% tic
% import Core.run_pval_matlab; % we only compute this for best stretch,
% [par1,par2,ccMax] = run_pval_matlab(len1, len2, w, 1, 1.88, 100);
% toc
end

