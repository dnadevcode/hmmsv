function [res] = compute_pvalues(res,sets)
    % 
    % if multiple comparisons, each will need a different p-value file
    % (unless many results in the same?)

    if isempty(sets.pval.pvalFile)
        import Pvalue.pregenerate_values
        sets.pval.pvalFile = pregenerate_values(length(res.bar1),length(res.bar2), res.lengths,sets.comparison.circ, sets.pval.kernelsigma,sets.pval.numBars,sets.output.outfold);
   end
    
    import Pvalue.pval_pregenerated;
    % for unique barcodes it's easy. 
    [res.pval] = arrayfun(@(x,y) pval_pregenerated(x,y,sets.pval.pvalFile,1),res.fragmentpcc,res.lengths);

%     import Core.run_pval_matlab;
%     [par1,par2,~,thresh] = arrayfun(@(x) run_pval_matlab(x, length(res.bar2),sets),res.lengths);
%     res.thresh = thresh;
%     res.pdist = [par1 par2];
%     % compute p-vals
%     pvalFun = @(x,y,z) 1-(0.5+vpa(0.5)*(vpa(1,16)-vpa(betainc(x.^2,0.5,y/2-1,'upper'),16))).^z;
%     res.pval = arrayfun(@(x,y,z) pvalFun(x,y,z),res.fragmentpcc,par1,par2);
        
    
    res.pass = res.pval < 0.01;

    
    
end

