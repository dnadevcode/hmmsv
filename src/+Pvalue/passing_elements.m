function [pass] = passing_elements(x,u)
    if isempty(x)
        pass =[];
        return;
    end
	pass =  x(ismember(x(:,6)',find(u<0.01)),:);
    
end

