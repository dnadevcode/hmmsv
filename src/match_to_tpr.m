function [ROCpre,ROCpost,pcc_table] = match_to_tpr(match_table,matchTable,bar1,bar2,len1,len2,pvalFile,allowedGap,settingsHMM)



        % 6. close gaps : todo: put in a nicer wrapping function
        import functions.merge_table_data; % use coordinates in cartesian coords for merging?
        % merge table, should be working for both linear and circular barcodes
        [mergedTable] = cellfun(@(x,y,z) merge_table_data(x,y,z,allowedGap),match_table,bar1,bar2,'un',false);
% %             
%             for i=1:length(bar1)
%                 [mergedTable{i}] =  merge_table_data(match_table{i},bar1{i},bar2{i},allowedGap);
%             end

        % 7. True Positive Rate before p-value)
        % for true positive calculation, we need to create fullTables for both
        % true table and merged table.

        % now compute tpr.
        % todo: vectorize
        import Stats.compute_tpr;
        tpr = cellfun(@(x,y,z,u) compute_tpr(x,y,z,u, settingsHMM),mergedTable,matchTable,len1,len2,'un',true);
        
%         import Stats.compute_tpr;
%          for i=1:length(bar1)
%             tpr(i) =  compute_tpr(mergedTable{i},matchTable{i},len1{i},len2{i}, settingsHMM);
%          end

%             
        ROCpre = nanmean(tpr);
%             ROCprec{idx,idy} = tpr;
% 
%             % seems ok
        import Comparison.pcc_for_clusters;
        [pcc_table,pcc_lengths] = cellfun(@(x,y,z) pcc_for_clusters(x,y,z),mergedTable,bar1,bar2,'un',false);
% %         % 
%             for i=1:length(bar1)
%              [pcc_table,pcc_lengths] = pcc_for_clusters(mergedTable{i},bar1{i},bar2{i});
%             end
%             
        % compute p-value
        import Pvalue.pval_pregenerated;
        % for unique barcodes it's easy. 
        [pccVal] = cellfun(@(x,u) pval_pregenerated(x, u, pvalFile),pcc_table,pcc_lengths,'un',false);
% 
%             %     for i=1:100
%         %         i
%         %         [pccVal] = pval_pregenerated(pcc_table{i},len1{i},len2{i},pcc_lengths{i},pvalFile);
%         %     end
% 
%             % now create new table only with those idx that pass the threshold
%         try
        import Pvalue.passing_elements;
        [passingElts] = cellfun(@(x,u) passing_elements(x,u),mergedTable,pccVal,'UniformOutput',false);

% one of these might be unempty
%         passingElts = cellfun(@(x,u) x(ismember(x(:,6)',find(u<0.01)),:),mergedTable,pccVal,'UniformOutput',false);
%         catch
%         end
% 
%             % pval after calculating thresh
        tprP = cellfun(@(x,y,z,u) compute_tpr(x,y,z,u, settingsHMM),passingElts,matchTable,len1,len2,'un',true);
        
        ROCpost = mean(tprP);
end

