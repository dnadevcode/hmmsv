function [theory, header, bitmask] = gen_om_theory(name,sets)
    % gen_om_theory / rewritten compute_theory_barcode function

    % if free conc. needs to be generated
%     import CBT.Hca.Core.Theory.compute_free_conc;
%     sets = compute_free_conc(sets);
    
    
    import CBT.Hca.Core.Theory.create_memory_struct;
    [chr1, header] = create_memory_struct(name);
      
    if length(chr1.Data) < sets.theoryGen.k
        disp(strcat(['Theory ' name ' skipped because length < theoryGen.k =' num2str(sets.theoryGen.k) ]));
        theory = [];  bitmask = [];
        delete(chr1.Filename);
        clear chr1;
        return;
    end
        
    
    % set-up-theory-generation

    [~, Y, cutPointsL, cutPointsR, extraL, extraR, ...
        ~, ~, k, m, circular, ~, ~, numPx,~, ~,bpPlaceL,bpPlaceR ] = ...
        set_up_theory_for_calculation(sets, chr1);

    % generate theory & bitmask/ using cutPointsL and cutPointsR
    [theory,bitmask] = generate_theory(Y, chr1, cutPointsL, cutPointsR, extraL, extraR, ...
     k, m, circular, numPx, sets);
    
    
    % clear chr1
    delete(chr1.Filename);
    clear chr1;
    
    
% end
% %% A C G T
% chr2.Data=ones(length(chr1.Data),1);
% chr2.Data((1:5)+5)=3;
% chr2.Data(40000:40004)=3;
% % chr2.Data(end-4:end)=3;
% 
% 
% % chr2.Data(1:end/2)=3;
%  [~, Y, cutPointsL, cutPointsR, extraL, extraR, ...
%         ~, ~, k, m, circular, ~, ~, numPx,~, ~,bpPlaceL,bpPlaceR ] = ...
%         set_up_theory_for_calculation(sets, chr2);
%     sets.theoryGen.method = 'simple';
% circular=0;
%     [theory,bitmask] = generate_theory(Y, chr2, cutPointsL, cutPointsR, extraL, extraR, ...
%      k, m, circular, numPx, sets);
% %  figure,plot(fftshift(theory))
% 
%  circular=1;
%     [theory2,bitmask] = generate_theory(Y, chr2, cutPointsL, cutPointsR, extraL, extraR, ...
%      k, m, circular, numPx, sets);
%  
%   figure,plot(fftshift(theory))
%  hold on
%  plot(fftshift(theory2))
% % plot(fftshift(theory))