function [fullTable,f] = plot_sv( barQ, barD, matchTable ,sets,outName,lgndString,bpPerPx)
    % plot_sv
    %
    % in this case allow the first barcode, the query, to be circular 
    % (since we probably want to do the same later. Or 
    %
    %
    %
    % plot the sample of simulation
    %
    %   Args:
    %      resultStruct,sets,name,qr
    %   Returns:
    %
    %   fullTable: structure with fulltable of data
    %
    %
    % detail differences between this and plot_sv_full..
    % this is used for example plot
    % schematic_figure

%     
fullTable=[];
    timestamp = datestr(clock(), 'yyyy-mm-dd_HH_MM_SS');

    if nargin < 6
        lgndString = [];
    end
    
    if nargin < 7
        bpPerPx = 500/1000;
        labelstr = 'Position (kb)';
    else
        labelstr = 'Position (px)';
    end

    % Plot figure
    f = figure('Position', [10 10 550 850])
    % Create full table from result table. TODO: test that this always
    % plots correctly
    import functions.create_full_table;
    
    bFig(1:length(matchTable)) = axes(f);
    numBars = length(matchTable);
    for idx=1:length(matchTable)
        name = sets.svType(idx);
        
        % mak a subplot, the number of subplots is the number of barcodes
        bFig(idx) = subplot(numBars,1, idx);

        ColOrd = get( bFig(idx), 'ColorOrder');
        % barcodes assumed to have no nan's in them and bitmasks of ones
        bar1 = zscore(barQ{idx});
        bar2 = zscore(barD{idx});
        res_table = matchTable{idx};

        ylmax = max(bar1); ylmin = min(bar1);  yumax = max(bar2+8); yumin = min(bar2+8);  
        hold on;

        h(1) = plot( bFig(idx), bar2, 'Color', 'black');
        hold on;
        h(2) = plot( bFig(idx), bar1+8, 'Color', 'black');


        % problem: both bar1 and bar2 can be circularly shifted..
        for i = 1:size(res_table, 1)
            [tempTable] = create_full_table(res_table(i,:), bar1,bar2);
            for j=1:size(tempTable,1)
                    pX = tempTable(j, [1 1 2 2 4 4 3 3]);
                    pY = [yumin yumax yumax yumin ylmax ylmin ylmin ylmax];
                    h(i+2) = patch(pX, pY, ColOrd(1+mod(tempTable(j,6), 7), :), 'faceAlpha', 0.1, ...
                          'edgeAlpha', 0.3, 'edgeColor', ColOrd(1+mod(tempTable(j,6), 7), :));
            end
        end

        hold off;
        title(name,'FontSize', 10,'Interpreter','latex')  ;
        set( bFig(idx),'XTick',[]);
        set( bFig(idx),'YTick',[]);
    end
    linkaxes(bFig, 'x')

    ticks = 1:50/bpPerPx:2*length(bar2);
    ticksx = floor(ticks*bpPerPx);
    bFig(end).XTick = [ticks];
    bFig(end).XTickLabel = [ticksx]; 
    xlabel( bFig(end),labelstr,'FontSize', 10,'Interpreter','latex')
    
    saveas(f,fullfile(sets.outfold,strcat(outName)),'epsc')
    fullfile(sets.outfold,strcat(outName))
end
