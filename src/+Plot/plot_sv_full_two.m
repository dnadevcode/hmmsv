function [fullTable] = plot_sv_full_two( resultStruct,sets,name,qr,plotonlypass,bpPerPx)
    % plot_sv
    %
    % in this case allow the first barcode, the query, to be circular 
    % (since we probably want to do the same later. Or 
    %
    %
    %
    % plot the sample of simulation
    %
    %   Args:
    %      resultStruct,sets,name,qr
    %   Returns:
    %
    %   fullTable: structure with fulltable of data
    %
    %
    

    if nargin < 6
        bpPerPx = 1;
        labelstr = 'Position (px)';
        str2 = 'px';
    else
        labelstr = 'Position (kbp)';
        str2 = 'kbp';
    end
%     
    if nargin < 4
        timestamp = datestr(clock(), 'yyyy-mm-dd_HH_MM_SS');
    else
        timestamp = 'test';
    end
%%
    % need to figure out the case when we allow circ = 1
    f = figure;
    
    import functions.create_full_table;
    
%     numBars = length(resultStruct);
%     for idx=1:length(resultStruct)
        try
        switch sets.svList
        case 1
            sets.svType = 'Insertion';
        case 2
            sets.svType = 'Invertion';
        case 3
            sets.svType = 'Repeat';
        case 4
            sets.svType = 'Translocation';
        otherwise
            error('wrong sv type');
        end
        catch
            sets.svType = num2str(1);
        end

        
        ax = subplot(1, 4,[1 2 3 4]);
%         ax = gca;

    ColOrd = get(ax, 'ColorOrder');
    bar1 = zscore(resultStruct{1}.bar1);
    bar2 = zscore(resultStruct{1}.bar2);
    bar3 =  zscore(resultStruct{2}.bar2);

    res_table = resultStruct{1}.matchTable;
    res_table2 = resultStruct{2}.matchTable;


    ylmax = max(bar1);
    ylmin = min(bar1)+4;
    yumax = max(bar2+16);
    yumin = min(bar2+16);  
    hold on;

    h(1) = plot(ax, bar2, 'Color', 'black');
    hold on;
    h(2) = plot(ax, bar1+16, 'Color', 'black');
    h(3) = plot( ax, bar3-16, 'Color', 'black');

    % number of fragments. TODO: keep an option of having different

    % fragments appear as a single fragment in output?
    numFragments = max(res_table(:,6));
    numFragments2 = max(res_table2(:,6));

    % convert to full table:
%     res_table
    import functions.create_full_table;
    res_table = create_full_table(res_table,bar1,bar2);
    res_table2 = create_full_table(res_table2,bar1,bar3);

%     if sets.output.merge
%         newTable = [];
%         for i=1:numFragments
%             subTable = res_table(find(res_table(:,6)==i),:);
%             [dSt,idSt] = min(subTable(:,1));
%             [dStop,idStop] = max(subTable(:,2));
%             newTable = [newTable; dSt dStop subTable(idSt,3) subTable(idStop,4) subTable(1,5:6)];
%         end
%         res_table = newTable;
%     end
    
    legendNames = cell(1,numFragments+numFragments2);
%     end
    fullTable = [];
%     N = length(bar1);    M = length(bar2);

    % problem: both bar1 and bar2 can be circularly shifted..
    for i = 1:size(res_table, 1)
        pX = res_table(i, fliplr([1 1 2 2 4 4 3 3]));
        pX = pX + [-0.5 -0.5 0.5 0.5 -0.5 -0.5 0.5 0.5];
        pY = [yumin yumax yumax yumin ylmax ylmin ylmin ylmax];
        pY = pY + [0 0.5 0.5 0 0 -0.5 -0.5 0 ];
        if resultStruct{1}.pass(res_table(i,6)) == 1
            h(i+2) = patch(pX, pY, ColOrd(1+mod(res_table(i,6), 7), :), 'faceAlpha', 0.1, ...
                  'edgeAlpha', 0.3, 'edgeColor', ColOrd(1+mod(res_table(i,6), 7), :));

        else
            if ~plotonlypass
               % this does not pass the thresh 
                h(i+2) = patch(pX, pY,ColOrd(1+mod(res_table(i,6), 7), :), 'faceAlpha',0, ...
                      'edgeAlpha', 0.5, 'edgeColor',ColOrd(1+mod(res_table(i,6), 7), :),'LineStyle','--');
            end
            % uint8([17 17 17])
        end
        
    end
  % figure
    ax = subplot(1, 4,[1 2 3 4]);

    for i=1:size(res_table2, 1)
        pX = res_table2(i, fliplr([1 1 2 2 4 4 3 3]));
%        ylmax = max(bar3);
%         ylmin = min(bar3);
%         yumax = max(bar3);
%         yumin = min(bar3);  
        pY2 = [yumin yumax-3 yumax-3 yumin ylmax ylmin-3 ylmin-3 ylmax]-16;
         if resultStruct{2}.pass(res_table2(i,6)) == 1

                h(2+size(res_table2, 1)) = patch(pX, pY2, ColOrd(numFragments+1+mod(res_table2(i,6),7), :), 'faceAlpha', 0.1, ...
                      'edgeAlpha', 0.3, 'edgeColor',ColOrd(numFragments+1+mod(res_table2(i,6), 7), :));
        else
            if ~plotonlypass
               % this does not pass the thresh 
                h(size(res_table2, 1)+2) = patch(pX, pY,ColOrd(numFragments+1+mod(res_table2(i,6), 7), :), 'faceAlpha',0, ...
                      'edgeAlpha', 0.5, 'edgeColor',ColOrd(numFragments+1+mod(res_table2(i,6), 7), :),'LineStyle','--');
            end
            % uint8([17 17 17])
         end 
    end
%
%         
    for i = 1:numFragments
        if resultStruct{1}.pass(i) == 1
            legendNames{find(res_table(:,6)==i,1,'first')} = strcat(['$C_{' num2str(i) '}$ = ' num2str(resultStruct{1}.fragmentpcc(i),3) ...
             ', $p_{' num2str(i) '}$ = ' char(vpa(resultStruct{1}.pval(i),3)) ...
             ', $l_{' num2str(i) '}$=' num2str(bpPerPx*resultStruct{1}.lengths(i)) str2 ]);
        else
            legendNames{find(res_table(:,6)==i,1,'first')} = strcat(['$C_{' num2str(i) '}$ = ' num2str(resultStruct{1}.fragmentpcc(i),3) ...
             ', $l_{' num2str(i) '}$=' num2str(bpPerPx*resultStruct{1}.lengths(i)) str2 ]);
        end
    end
    
         
    for i = 1:numFragments2
        if resultStruct{2}.pass(i) == 1
            legendNames{size(res_table,1)+find(res_table2(:,6)==i,1,'first')} = strcat(['$C_{' num2str(i) '}$ = ' num2str(resultStruct{2}.fragmentpcc(i),3) ...
             ', $p_{' num2str(i) '}$ = ' char(vpa(resultStruct{2}.pval(i),3)) ...
             ', $l_{' num2str(i) '}$=' num2str(bpPerPx*resultStruct{2}.lengths(i)) str2 ]);
        else
            legendNames{size(res_table,1)+find(res_table2(:,6)==i,1,'first')} = strcat(['$C_{' num2str(i) '}$ = ' num2str(resultStruct{2}.fragmentpcc(i),3) ...
             ', $l_{' num2str(i) '}$=' num2str(bpPerPx*resultStruct{2}.lengths(i)) str2 ]);
        end
    end

        
    hold off;
%     if plotonlypass 
%         legend(h(logical([0 0 resultStruct.pass])),legendNames(resultStruct.pass),'Interpreter','latex','Location', 'southoutside')
%     else
    clusters = find(~cellfun(@isempty,legendNames));
    legend(h(clusters+2),legendNames(clusters), 'Interpreter' ,'latex', 'Location', 'southoutside')
%     end
    ylabel('Shifted barcode intensities','FontSize', 10,'Interpreter','latex');
%     xlabel('Position','FontSize', 10,'Interpreter','latex');
%     title(sets.svType,'FontSize', 10,'Interpreter','latex')  ;
% 
    ticks = 1:50/bpPerPx:2*length(bar2);
    ticksx = floor(ticks*bpPerPx);
    ax.XTick = [ticks];
    ax.XTickLabel = [ticksx];   
    xlabel( ax,labelstr,'FontSize', 10,'Interpreter','latex')

    saveas(f,fullfile(sets.output.figsfold,strcat(timestamp,name)),'epsc')
    fullfile(sets.output.figsfold,strcat(timestamp,name))
% 
%     f = figure;

%     ax = subplot(1,1,1);
%     [tempTable,barfragq,barfragr] = create_full_table(res_table,bar1,bar2',1);
%         
%     for i=1:length(barfragq)
%          if resultStruct.pass(i) == 1 || ~plotonlypass  
%             plot( zscore(barfragq{i})+i*5,'color',ColOrd(1+mod(i, 7), :))
%             hold on 
%             plot(zscore(barfragr{i})+i*5,'black')
%          end
%     end
%     ticks = 1:50/bpPerPx:2*length(bar2);
%     ticksx = floor(ticks*bpPerPx);
%     ax.XTick = [ticks];
%     ax.XTickLabel = [ticksx];   
%     xlabel( ax,labelstr,'FontSize', 10,'Interpreter','latex')
% 
% 
%     saveas(f,fullfile(sets.output.figsfold,strcat(timestamp,strcat('2',name))),'epsc');
