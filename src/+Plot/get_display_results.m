function [] = get_display_results(res,settingsHMM)
    bppx = 0.5;
    % display results for hmmsv
    settingsHMM.output.twoInOne = 0;
    if settingsHMM.output.twoInOne
        % plot every two barcode comparisons? in singl eplot
        import Plot.plot_sv_full_two;

%         for i=1:length(res)
        plot_sv_full_two( res,settingsHMM,strcat(num2str(i),settingsHMM.name),2,0,0.609);
    else
        import Plot.plot_sv_full;
        for j=1:length(res)
            if isfield(settingsHMM,'estlength')
                bppx= settingsHMM.estlength/length(res{j}.bar1);
            else
                bppx= 215/length(res{j}.bar2);
            end
            plot_sv_full( res{j},settingsHMM,strcat(num2str(j),settingsHMM.name),2,0,bppx);
        end
    end

end