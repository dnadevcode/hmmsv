function [res,settingsHMM] = hmmsv_run(settingsHMM)
% runs hmmsv method to detect pair motifs between experimental/theoretical
% DNA barcodes.


    % if settings were not provided, load them from default
    if nargin < 1 
        settingsHMM = ini2struct( 'hmmsv_default_settings.txt');
    end

    % timestamp for the results
    timestamp = datestr(clock(), 'yyyy-mm-dd_HH_MM_SS');
    settingsHMM.timestamp = timestamp;

    import Import.get_user_settings;
    settingsHMM = get_user_settings(settingsHMM);
    

    % fix the random ring for reproducibility
    rng(1,'twister');
    
    % prepare data for the HMM analysis, these are queries.
    import Import.load_data;
    [queryAll] = load_data(settingsHMM.query,settingsHMM,'query');          
    [dataAll] = load_data(settingsHMM.data,settingsHMM,'data');     
    
%     queryAll{1}.barcode = fliplr(queryAll{1}.barcode);% temp flip
%     queryAll{1}.bitmask = fliplr(queryAll{1}.bitmask);

    % compare query vs. data
    import Method.compare_query_data;
    res = compare_query_data(queryAll,dataAll,settingsHMM);
    
%     rng(1,'twister');

    import Pvalue.compute_pvalues;
    res = cellfun(@(x) compute_pvalues(x,settingsHMM),res,'un',false);
%     
%     for i=1:length(res)
%         res{i} = compute_pvalues(res{i},settingsHMM);
%     end

    % Plot results for all comparisons
    import Plot.get_display_results;
    get_display_results(res,settingsHMM);

    assignin('base','res', res)
    assignin('base','queryAll', queryAll)
    assignin('base','dataAll', dataAll)
    assignin('base','settingsHMM', settingsHMM)


    
    % add additional calculations/interpretation of data
    %     import Export.additional_computations;
    %     additional_computations(res,queryAll,dataAll,settingsHMM );
        

   
    

    
        
end

