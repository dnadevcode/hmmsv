#
# HMMSV settings
#
askfordata = 0; % whether to ask for input data
askforsets = 0; % if to ask for settings
estlength = 80;
samelength = 1;
[query]
dataFile = 'src/settings/query.txt';
type = 'consensus';
[data]
dataFile = 'src/settings/data.txt';
type = 'consensus';
[output] 
askforoutputdir = 0;
merge = 1;                                       % If to merge fragments belonging to the same cluster in the output
outfold = 'output';
figsfold = 'output';
[comparison]
dataLenLimit = 1000                              % Length limit comparison to find the locus
stretch =  [0.9 1.1];                         % rescaling factors for determining stretch
stretchPx = 2;                                   % compare every 5 pixels for stretch detection
stretchF = 0.02;                                 % rescaling factor for HMM around the expected factor
r = 50;                                         % window width for determining stretch
k = 2^14;                                        % data splitting parameter for MP numerics
circ = 1;                                        % whether interpret data as circular
interpolationMethod = 'linear';                  % interpolation method
bashscript = 'src/bash';                         % location of bash script
c = 20;                                          % length threshold for the method
alfabet_size = 60;                               % alphabet size for discretized random data
% G = 1/alfabet_size;
alfabet_sigma = 1;                               % sigman for the alphabet
C_M = 0.057; % 2log10(p_MG)< log10(p_GG)         % p_MJ probability 1/2
C_G = 17.61;                                     % P_JJ probability 1/3
cg = 0;                                          % gap constraint
cscript = 'src/c/hmm.out';                       % bash script which runs the hmm
allowedGap = 5;                                  % allow gap between rows of result table
st = 1;                                          % different between positions to be called true match
[pval]
kernelsigma = 300/159.2;
pvalFile = 'test/pval_pvalpvalblasmall.mat';     % test p-value file
numBars = 1000;                                  % number of random barcodes to generate for p-value fit
thresh = 0.01;                                   % thresh for p-value 