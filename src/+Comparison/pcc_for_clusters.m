function [pcc_table,pcc_lengths] = pcc_for_clusters(mergedTable,bar1,bar2withNoise)

    % Args:
    %       mergedTable - table with output results
    %       bar1 - query barcode
    %       bar2.. - data barcode
    
    %   Returns:
    %       pcc_table - table with pccvalues,
    %       pcc_lengths - table with pcc comparison lengths
    
    if isempty(mergedTable);
        pcc_table =[];
        pcc_lengths =[];
        return;
    end
    import Comparison.pcc;
    
    import functions.table_to_bar;
    [bar1frag, bar2frag, bitmask1, bitmask2] = arrayfun(@(x) table_to_bar(mergedTable(x,:),bar1,bar2withNoise),1:size(mergedTable,1),'UniformOutput',false);
    	
    uniqueClusters = unique(mergedTable(:,6));
    numCl = length(uniqueClusters);
    
    pcc_table = zeros(1,length(numCl));
    pcc_lengths= zeros(1,length(numCl));
    for i =1:numCl
        curIdx = mergedTable(:,6) == uniqueClusters(i);
        newBar1 = cell2mat(bar1frag(curIdx));
        newBar2 = cell2mat(bar2frag(curIdx));
        pcc_table(i) = pcc(newBar1,newBar2);
        pcc_lengths(i) = length(newBar1);
    end

end

