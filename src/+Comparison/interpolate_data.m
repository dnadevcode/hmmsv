function [y] = interpolate_data(query,lenQ,newlenQ,sets)
    % intepolate data
    % Old vector is at discret points 0:lenQ+1
    % new vector must have newlenQ points. they don't
    % necessarily start at 1 and end at lenQ (no need to have
    % these two points fixed.. instead, we interpolate so that
    % the interpolation length first-last would be lenQ-1, but
    % endpoints can change a bit
    %                 Xp = linspace(1,lenQ+1,newlenQ+1);
    %                 
    %                 % so we make sure all have same gaps in between
    %                 y = interp1(circQuery,Xp(1:end-1));

   
    switch sets.comparison.interpolationMethod
        case 'sinc'

            % original interval
            t = 1:lenQ;

            ts = linspace(1,lenQ,newlenQ);
            [Ts,T] = ndgrid(ts,t);
  
            y = sinc(Ts - T)*query;            
        case 'linear' % could just use imresize? / alt same interpolation as when running phase rand?
%             y = interp1(query,ts);
            % maybe we want extrapolate edge points? Especially if data is
            % circular!
%             sets.circular =1;
%             if sets.circular
                % add two points to work as 0 and N+1
            circQuery = [query query(1)]; %add extra point since it's circular
%             else % just repeat the last entry..
%                 circQuery = [query query(end)];
%             end

            grid =  1:round(newlenQ+1);
            Xp2 = arrayfun(@(x) 1+(x-1)*lenQ/newlenQ,grid);
            y = interp1(circQuery,Xp2(1:end-1));
%             end
        case 'lin2'
               circular =1;
            if circular
                % add two points to work as 0 and N+1
                circQuery = [query query(1)]; %add extra point since it's circular
                % Old vector is at discret points 0:lenQ+1
                % new vector must have newlenQ points. they don't
                % necessarily start at 1 and end at lenQ (no need to have
                % these two points fixed.. instead, we interpolate so that
                % the interpolation length first-last would be lenQ-1, but
%                 endpoints can change a bit
                Xp = linspace(1,lenQ+1,newlenQ+1);
                
                % so we make sure all have same gaps in between
                y = interp1(circQuery,Xp(1:end-1));
                
%                 grid =  1:round(newlenQ);
%                 Xp2 = arrayfun(@(x) 1+(x-1)*lenQ/newlenQ,grid);
%                 y = interp1(circQuery,Xp2(1:end-1));

            end
        case 'lin1'
                g = @(x,y) interp1(x,1:1/y:length(x));
                y = g(query,newlenQ/lenQ);
            
            
        otherwise
            error('wrong interpolation method selected');
    end
    % use ideal interpolation, i.e. sinc
    %         plot(t,data,'o',ts,y)

    
    %
    %         % use ideal interpolation, i.e. sinc
%         ts = linspace(1,lenD,newlenD);
%         [Ts,T] = ndgrid(ts,t);
%         y = sinc(Ts - T)*data;
%         plot(t,data,'o',ts,y)
%         xlabel Time, ylabel Signal
%         legend('Sampled','Interpolated','Location','SouthWest')
%         legend boxoff
end

