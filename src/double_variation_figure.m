function [res,settingsHMM] = double_variation_figure(pval,svtype,t1,t2)

rng(1,'twister');

sets = ini2struct( 'default_synthetic_settings.txt');

if nargin >=2
   sets.svType = svtype;
end



bar1{1} = normrnd(0,1,1,sets.len1);

import Rand.single_sv;
[b2,mT] = arrayfun(@(x) single_sv(bar1{1}, 50, x,  sets.sigma,sets.circ),[t1] ,'un',false);
bar2{1} = b2{1};matchTable{1} = mT{1};
[b2,mT] = arrayfun(@(x) single_sv(bar2{1}, 50, x,  sets.sigma,sets.circ),[t2] ,'un',false);
bar2{2} = b2{1};matchTable{2}=mT{1};
import Rand.add_psf_sv;
[bar1] = cellfun(@(x) add_psf_sv(x,1, 1.8844),bar1 ,'un',false);
[bar2] = cellfun(@(x) add_psf_sv(x,1, 1.8844),bar2 ,'un',false);

import functions.transitive_match_table;
 [finalPos,matchT] = transitive_match_table(matchTable,bar1,bar2);
 matchTableNew = matchTable;
 matchTableNew{3} = matchT;
import Comparison.pcc;
if sets.noise~= 0
    barRnd = normrnd(0, 1, 1, length(bar2{end}));        
    barRnd =  imgaussfilt(barRnd,  sets.sigma);
    tf = @(x) pcc( bar2{end},(1-x)* bar2{end}+barRnd*x)-(1-sets.noise);
    x0 = 0.5;  x = fzero(tf,x0);
    bar2{end} = (1-x)* bar2{end}+barRnd*x;
end


sets.svType = { '(A) Inversion', '(B) Translocation', '(C) Combination'};

 % Fig1-eps-converted-to.pdf
sets.outfold = 'output';
% import Plot.plot_sv;
% plot_sv( [bar1,bar2{1},bar1], [bar2 bar2{end}],matchTableNew,sets,'Figgg.eps');

% % first generate some synthetic data..
% import Rand.gen_synthetic_sv;
% [bar1,bar2,matchTable] = gen_synthetic_sv(sets.len1,sets.len2, sets.svType, sets.circ, sets.sigma,sets.noise);

% % maybe plot this to check?
% import Plot.plot_sv;
% plot_sv( bar1, bar2,matchTable,sets,'Fig1.eps');

settingsHMM = ini2struct( 'hmmsv_default_settings.txt');
% set up output dir path
mkdir(settingsHMM.output.outfold);

if nargin>=1
%     settingsHMM.pval.pvalFile = pval;
    splitName = strsplit(pval,'_');
    splitName{end-5} = num2str(length(bar2{end}));
    splitName{end-6} = num2str(length(bar1{1}));
    settingsHMM.pval.pvalFile = strjoin(splitName,'_');
else
    % pregenerate
end

query = {fullfile(settingsHMM.output.outfold,strcat(['bar1_query_' num2str( sets.circ) '_.txt']))};
fd =fopen(query{1},'w');   fprintf(fd,"%16.15f ", bar1{1});    fclose(fd);
data = {fullfile(settingsHMM.output.outfold, strcat(['bar1_data_' num2str( sets.circ) '_.txt']))};
fd =fopen(data{1},'w');   fprintf(fd,"%16.15f ", bar2{end});    fclose(fd);
tableData = fullfile(settingsHMM.output.outfold, 'bar1_matchTable.txt');
fd =fopen(tableData,'w');   fprintf(fd,"%5d ", matchT);    fclose(fd);
        
% now save bar1 bar2 matchTable and settings into txt files
% Example how to run consensus vs consensus for hmmsv. Single query
% and single data

% query = repmat({'/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/P1K0.mat'},3,1);
% data = {'/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/P3K0.mat',...
%     '/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/P5K0.mat',...
%     '/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/P8K0.mat'};

settingsHMM.query.type = 'synthetic';
settingsHMM.query.dataFile = 'src/settings/query.txt';

settingsHMM.data.type = 'synthetic';
settingsHMM.data.dataFile = 'src/settings/data.txt';

import Import.create_txt_file;
create_txt_file(settingsHMM.query,query);
create_txt_file(settingsHMM.data,data);

% figure name
settingsHMM.output.figsfold = 'output/';
settingsHMM.name = strcat(['double_var' num2str(svtype) '.eps']);
settingsHMM.estlength = length( bar2{end})/2;
% for p-value pregeneration, here we re-use the same fie from determine
% noise dependence. We also allow for re-scaling here, should we take that
% into account when computing p-value?

[res,settingsHMM] = hmmsv_run(settingsHMM)

end

