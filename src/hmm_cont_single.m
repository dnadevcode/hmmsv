function [match_table,pathV] = hmm_cont_single(queries,nameData,settingsHMM,foldToSaveData,circ)

    settingsHMM.comparison.cscript = 'src/c/test/hmm_console.out';
 
    % version new:
        par =   { settingsHMM.comparison.cscript,...
            num2str(settingsHMM.comparison.C_M),...
            num2str(settingsHMM.comparison.C_G),...
            num2str(settingsHMM.comparison.c),...
            num2str(settingsHMM.comparison.cg),...
            num2str(circ)};
    
        cmdStr2       = [fullfile(settingsHMM.comparison.bashscript,'generate_sv_single.sh') ' ' par{1} ' ' queries ' ' nameData ' ' par{2} ' ' par{3} ' ' par{4} ' ' par{5} ' ' par{6}];
        system(cmdStr2);
        
        import Import.load_hmm_output;
        [match_table,pathV] =  load_hmm_output(queries);

%         for i=1:length(queries)
%             [match_table{i},pathV{i}] = load_hmm_output(queries{i});
%         end
end

