function [match_table,score,rescaledbars,comparisonStruct,scoreCombined,stretchStruct] = hmmsv(bar1, bar2, bar2txt, sets, bestStretch, name)
    %
    %   Args:
    %       bar1: barcode 1
    %       bar2: barcode 2
    %       sets,name
    %
    %   Returns:
    %       resultStruct
    %
    
    lenQ = length(bar1);

    stretchFactors = [lenQ*(bestStretch-sets.comparison.stretchF):1:lenQ*(bestStretch+sets.comparison.stretchF)]/lenQ;
    sets.analysis.stretchFactors = stretchFactors;
    % run bash hmm script for randStruct, and output the result tables 
    import functions.create_full_table;
    import functions.convert_matchtable;
    import Comparison.interpolate_data;

    pathToScript = fullfile(sets.comparison.bashscript,'c_hmm_script.sh'); 

%     par =   {num2str(sets.alfabet_size), num2str(sets.circ),num2str(sets.alfabet_sigma),num2str(sets.C_M),num2str(sets.C_G),num2str(sets.c), num2str(sets.cg),sets.cscript};
%     cmdStr       = [pathToScript ' ' par{1} ' ' par{2} ' ' par{3} ' ' par{4} ' '  par{5} ' ' par{6} ' ' par{7} ' ' par{8} ' ' name];
%     system(cmdStr);
%     sets.fold = strcat(num2str(name),'dataSim/');
%     sets.foldResult = strcat(num2str(name),'resultData/');
% 
% %     if ~exist( sets.fold) or  ~exist( sets.foldResult)
%     mkdir( sets.fold);
%     mkdir( sets.foldResult);
    % lenght query
%     sets.circ  =1;

    % shouldn't have to save the data everytime..! change the script a
    % little
%     mkdir(sets.output);
    nameData = fullfile(sets.output.outfold,sets.timestamp, strcat([num2str(name) '_data.txt']));
    if sets.circ
        fd =fopen(nameData,'w');   fprintf(fd,"%16.15f ", [bar2 bar2(1:sets.comparison.c-1)]);    fclose(fd);
        totLen = length(bar2)+sets.comparison.c-1;
    else
        fd =fopen(nameData,'w');   fprintf(fd,"%16.15f ", bar2); fclose(fd);
        totLen = length(bar2);
    end

    match_table = cell(1,length(stretchFactors));
    score = zeros(1,length(stretchFactors));
    rescaledbars = cell(1,length(stretchFactors));
    comparisonStruct = cell(1,length(stretchFactors)); % lengths and pcc scores for all fragments
    for j=1:length(stretchFactors)
        % interpolate
        [y] = interpolate_data(bar1,lenQ,lenQ*stretchFactors(j),sets);
        rescaledbars{j} =y;
        % save as txt
        nameQuery = fullfile(sets.output.outfold,sets.timestamp, strcat([num2str(name) '_' num2str(j),'_query.txt']));
        fd =fopen(nameQuery,'w');   fprintf(fd,"%16.15f ", y);    fclose(fd);
 
        % todo: remove the hardcoding..
        pM = 0.51;
        pG = 0.31;
        sets.comparison.C_M = 1/(1-pM)-1;
        sets.comparison.C_G = 2*length(y)*(1/(1-pG)-1);
    
%         pathToScript = fullfile(sets.bashscript,'c_script2.sh'); 
        [match_table{j},~] = hmm_cont_single(nameQuery,nameData,sets,[],sets.circ);
%         match_table{1}
%         
%         par =   {num2str(sets.comparison.alfabet_size), num2str(sets.circ),num2str(sets.comparison.alfabet_sigma),num2str(sets.comparison.C_M),num2str(sets.comparison.C_G),num2str(sets.comparison.c), num2str(sets.comparison.cg),sets.comparison.cscript};
%         cmdStr       = [pathToScript ' ' par{1} ' ' par{2} ' ' par{3} ' ' par{4} ' '  par{5} ' ' par{6} ' ' par{7} ' ' par{8} ' ' nameQuery   ' ' nameData];
%         system(cmdStr);

%         resultName = fullfile(strcat(num2str(name),'resultData'), strcat([num2str(name) '_' num2str(j),'_query.txt']));
        import Import.load_hmm_output;
        match_table{j} =load_hmm_output(nameQuery);
   
%         A = importdata(strrep(nameQuery,'query','query_output'),' ',2);
%         try
%             rs.matchTable = A.data;
%         catch
%             rs.matchTable = [];
%         end
        % note that we should do this for MATLAB too. Don't convert!
%         match_table{j} = rs.matchTable; %convert_matchtable(rs);

        % in this case there might be too much pixels for data
%         if sets.circData
            % concerns first and last fragments, they might need to be
            % merged

                % 6. close gaps : todo: put in a nicer wrapping function
%         import functions.merge_table_data;
%         % merge table, should be working for both linear and circular barcodes
%         [mergedTable] = merge_table_data(resultStruct{bestSt},rescaledbars{bestSt},data.barcode,sets.comparison.allowedGap);
% 
% % 
%         % how does full table look if we include circularity for data
%         % now we should extract these fragments
%         [fullTable,barqf,bardf] = create_full_table(match_table, y,bar2',1);
%                
        dataLoopPixels = match_table{j}(1,2) - length(bar2);
       match_tableTemp =  match_table{j};
        if dataLoopPixels>0
            % remove extra pixels from data.
            if match_tableTemp(1,5)==1
                match_tableTemp(1,1:5) = [ match_tableTemp(1,1) match_tableTemp(1,2)-dataLoopPixels ...
                    match_tableTemp(1,3) mod(match_tableTemp(1,4)-dataLoopPixels-1,length(y))+1 match_tableTemp(1,5)];
            else
                match_tableTemp(1,1:5) = [ match_tableTemp(1,1) match_tableTemp(1,2)-dataLoopPixels ...
                    match_tableTemp(1,3) mod(match_tableTemp(1,4)+dataLoopPixels-1,length(y))+1 match_tableTemp(1,5)];
            end
        end
    
    
        import Comparison.pcc_for_clusters;
        [ comparisonStruct{j}.pcc_table, comparisonStruct{j}.lengths] = pcc_for_clusters(match_tableTemp,rescaledbars{j},bar2);
% % 
       % This works both for circular and linear barcodes
%        import functions.table_to_bar;
%        [barqf, bardf,bit1,bit2] = arrayfun(@(x) table_to_bar( match_table{j}(x,:),y,bar2),1:size( match_table{j},1),'UniformOutput',false);
% 
% %         % a bit hard to compare with the matlab version since that
% %         % one at the moment reports the PCC for discrete data
%         comparisonStruct{j}.pcc_table = cellfun(@(x,u) zscore(x,1)*zscore(u',1)/length(x),barqf,bardf);
% %         comparisonStruct{j}.aligment_table = match_table;
% %         comparisonStruct{j}.fullTable = fullTable;
%         comparisonStruct{j}.lengths = cellfun(@(x) length(x),barqf);
%         try
%             score(j) = str2double(strrep(A.textdata{1},'Score',''));
%         catch
%              score(j)  = -inf;
%         end
    end
    % what if we take longest: / because otherwise bad ones influence too
    % much
    [a,b] = cellfun(@(x) max(x.lengths),comparisonStruct);
    scoreCombined = arrayfun(@(x,y,z) comparisonStruct{x}.pcc_table(y)*z, 1:length(comparisonStruct),b,a);

    scoreCombined2  =  cellfun(@(x) sum(x.pcc_table.*x.lengths)/sum(x.lengths),comparisonStruct);  
%     f= figure,subplot(2,1,1);plot(sets.analysis.stretchFactors,scoreCombined2); legend({'$\max_{l_i} l_i \cdot dist_i$'},'Interpreter','latex')
%     subplot(2,1,2)
%     plot(sets.analysis.stretchFactors,scoreCombined);hold on;plot(1,0,'redx');legend({'$(\sum l_i \cdot dist_i)/(\sum l_i )$','True re-scaling factor'},'Interpreter','latex')
%     xlabel('Re-scale factor');ylabel('score')
%     saveas(f,'figs/rescalingchoise.eps','epsc')
    [bestpcc,idx] =  max(scoreCombined);
    bestSt = sets.analysis.stretchFactors(idx);
    stretchStruct.bestSt = bestSt;
    stretchStruct.stretchFactors = sets.analysis.stretchFactors;
    stretchStruct.idx = idx;
    stretchStruct.scoreCombined2 = scoreCombined2;
    stretchStruct.scoreCombined = scoreCombined;

    % now remove
	system(strcat(['rm ' nameData]));
    system(strcat(['rm ' fullfile(sets.output.outfold,sets.timestamp, strcat([num2str(name) '*' '_query.txt']))]));
    system(strcat(['rm ' fullfile(sets.output.outfold,sets.timestamp, strcat([num2str(name) '*' '_query_output.txt']))]));

%     system(strcat(['rm ' strrep(fullfile(sets.output, strcat([num2str(name) '*' '_query.txt'])),'dataSim','resultData')]));
    % 

    % maybe instead select best stretch based on where score is highest ?

%     import Comparison.select_best_comparison_stretch_factor;
%     [resultStruct] = select_best_comparison_stretch_factor(comparisonStruct,sets);
%     
%     resultStruct.bar1 = interpolate_data(bar1,lenQ,lenQ*resultStruct.bestSt,sets);
%     resultStruct.bar2 = bar2;
%     resultStruct.matchTable = resultStruct.comparisonStruct.aligment_table;
%     

end

