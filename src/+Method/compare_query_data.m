function [res] = compare_query_data(queryAll,dataAll,sets)
   disp('Starting comparing query to data...')
    tic
    
    % save output here
    res = cell(1,length(queryAll));

    for barNr = 1:length(queryAll)
        disp(strcat(['comparing query '  queryAll{barNr}.fname ' to data ' dataAll{barNr}.fname] ));
        
        import Method.on_compare;
        res{barNr} = on_compare(queryAll{barNr},dataAll{barNr},sets,barNr);
%         
%         if isequal(comparisonMethod,'mpAll')
%             import CBT.Hca.Core.Comparison.on_compare_mp_all;
%             [rezMax{barNr},bestBarStretch{barNr},bestLength{barNr},rezMaxAll{barNr}] = on_compare_mp_all(barcodeGen,theoryStruct{barNr},comparisonMethod,stretchFactors,w,numPixelsAroundBestTheoryMask);
%         else
%             import CBT.Hca.Core.Comparison.on_compare;
%             [rezMax{barNr},bestBarStretch{barNr},bestLength{barNr}] = on_compare(barcodeGen,theoryStruct{barNr},comparisonMethod,stretchFactors,w,numPixelsAroundBestTheoryMask);
%         end
%         % % 
% %         import CBT.Hca.Core.Comparison.on_compare_theory_to_exp;
% %         comparisonStruct{barNr} = on_compare_theory_to_exp(barcodeGen,theoryStruct{barNr}, sets);
    end
    
      
    
    
    
    timePassed = toc;
    disp(strcat(['Comparison finished in ' num2str(timePassed) ' seconds']));


end

