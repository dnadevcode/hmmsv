function [res] = on_compare(query,data,sets,barNr)
    % runs comparison between query and data using parameters form sets
    % file.
    
    % Split method into a few separate modules/functions that can be 
    % combined here.
    
    sets.circ = query.circ;
    
%     if sets.circ==0
%         data.barcode = data.barcode(data.bitmask); 
%     end
    
    if length(data.barcode) > sets.comparison.dataLenLimit
        % find the best location given this length..
    else
        if isfield(sets,'samelength' )
            bestStretch = 1;
            passthresh = 1;
            maxCoef = 1;
        else
            import mp.determine_stretch_factor; % with the length re-scaling, we're doing a little overfitting, i.e. we stretch to incorrect length 
            [bestStretch,passthresh,maxCoef] = determine_stretch_factor(query,data,sets,1);
        end
    end
%     passthresh=1; % ignore
    % 1. Rescale barcode length // todo: if the data barcode is long,
    % use this also to zoom in on relevant subfragment of data. Then the
    % position of the best stretch factor is important as well.
    if passthresh
        % 2-3-4 run HMMSV
        import Method.hmmsv;
        [resultStruct, scores, rescaledbars, comparisonStruct, scoreCombined, rescalingStruct] = hmmsv(query.barcode,  data.barcode,data, sets, bestStretch, num2str(barNr));

        % 5 select best rescaling factor
        bestSt = rescalingStruct.idx;


        % todo: if 1st is inversion, then flip one of the barcodes..
        import functions.flip_table;
        [resultStruct{bestSt},rescaledbars{bestSt}] = flip_table(resultStruct{bestSt},rescaledbars{bestSt});
               
        
        % 6. close gaps : todo: put in a nicer wrapping function
        import functions.merge_table_data;
        % merge table, should be working for both linear and circular barcodes
        [mergedTable] = merge_table_data(resultStruct{bestSt},rescaledbars{bestSt},data.barcode,sets.comparison.allowedGap,sets.circ);

%         % todo: if data is linear, have to check first and last
%         if sets.circ==0
%             
%         end
        % for consistency, check for overlapping data in results.

        import Comparison.pcc_for_clusters;
        [res.fragmentpcc, res.lengths] = pcc_for_clusters(mergedTable,rescaledbars{bestSt},data.barcode);
        % % 
        res.bar2 = rescaledbars{bestSt};
        res.bar1 = data.barcode; % what if this barcode has gaps in it's bitmask?
        res.matchTable = mergedTable;
        res.maxCoef = maxCoef; % where max coeff was attained
        res.bestSt = bestSt;
        res.rescalingStruct =rescalingStruct;
        else
            res.matchTable = [];
            res.maxCoef = maxCoef;
        end
    % 7. Use the P-value threshold to filter out remaining unlikely match
    % pairs. todo: put in a nicer wrapping function & allow pregeneration
%     sets.lenVar= length(res.bar1);
 
    % if p-value file pregenerated (for given length, sets.c to the found
    % length), then we use p-value pregenerated
%     import Pvalue.pval_pregenerated;
%     % for unique barcodes it's easy. 
%     [res.pval] = pval_pregenerated(res.fragmentpcc,length(res.bar2),length(res.bar1),res.lengths,sets.pval.pvalFile);
%     catch
    % if file not pregenerated, compute here
% end

 

end

