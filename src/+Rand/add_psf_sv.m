function [bar1out] = add_psf_sv(bar1,circ,sigma)

%     if ~prePsf
    bar1 = bar1-min(bar1);
%     bar2 = bar2-min(bar2);

    if circ(1)
        bar1 = [bar1(end-9:end) bar1 bar1(1:10)];
%         bar2 = [bar2(end-9:end) bar2 bar2(1:10)]; 
    else
        bar1 = [zeros(1,10) bar1 zeros(1,10)];
%         bar2 = [zeros(1,10) bar2 zeros(1,10)]; 
    end
    % todo: make this the same as for p-value calculation. One of the
    % barcodes could as well be linear.
    bar1 =  imgaussfilt(bar1,sigma);
%     bar2 =  imgaussfilt(bar2,sigma);
%         if circ(1)
    bar1out = bar1(11:end-10);
%     bar2out = bar2(11:end-10);            
% 
% alternative as in paper
% imgaussfilt(bar1,'psf'.circular)

end

