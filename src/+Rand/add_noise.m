function [bar2] = add_noise(bar2,sigma,noise)
    % add noise to bar2 using sigma and noise level noise (smaller noise -
    % more similar the barcodes
    import Comparison.pcc;

    barRnd = normrnd(0,1,1,length(bar2));        
    barRnd =  imgaussfilt(barRnd,sigma);
    tf = @(x) pcc( bar2,(1-x)* bar2+barRnd*x)-(1-noise);
    x0 = 0.5;  
    x = fzero(tf,x0);
    bar2 = (1-x)* bar2+barRnd*x;
end
% ?? 
% % convolving with psf changes the data to be a sum of gaussians 
% b1 = normrnd(0,1,1,1000000);
% % b1= normrnd(1,1000);
% % b1(500)=1;
% % std(b1,1)
% b2 = imgaussfilt(b1,sigma);
% mean(b2)
% std(b2,1)
% % mean(b2)
% std(b2,1).^2
% 1/(2*sqrt(pi)*sigma)
% %

