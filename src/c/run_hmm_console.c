/***********************************************************************/
/************************* DISCLAIMER **********************************/
/***********************************************************************/
/**                                                                   **/
/** Unless stated otherwise, all software is provided free of charge. **/
/** As well, all software is provided on an "as is" basis without     **/
/** warranty of any kind, express or implied. Under no circumstances  **/
/** and under no legal theory, whether in tort, contract,or otherwise,**/
/** shall Albertas Dvirnas be liable to you or to any other           **/
/** person for any indirect, special, incidental, or consequential    **/
/** damages of any character including, without limitation, damages   **/
/** for loss of goodwill, work stoppage, computer failure or          **/
/** malfunction, or for any and all other damages or losses.          **/
/**                                                                   **/
/** If you do not agree with these terms, then you you are advised to **/
/** not use this software.                                            **/
/***********************************************************************/
/***********************************************************************/

/** edited 27/01/2021. Add mex function so that this could be mexed and run
in matlab  **/

// these functions will change a little bit
// based on what we want

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>
// #include <iostream>
//#include "mex.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

// This function re-creates algorithm for computing MSV profile optimal score and
// path
// mexFunction helps to convert between C and matlab (i.e. we can include compiled C functions into matlab)

// https://stackoverflow.com/questions/252780/why-should-we-typedef-a-struct-so-often-in-c

//redefine these terms. Should be names of txt files
#define QSEQUENCE argv[1]
#define DSEQUENCE argv[2]
#define INF -1e20 // pseudo negative infinite number


// struct emissions {
//     double *B;
//     double *G;
// };

struct msvprofile {
    int length;
    int cM; // c match
    int cG; // c gap
    double *M; // transitions M
    double *G; // transitions G
};

// struct initial {
//     double *B;
// };
//
// struct msvprofile {
//     int length;
//     int *c; // k-minimum length constraints c
//     // struct emissions em;
//     struct transitions tr;
//     // struct initial it; not relevant if all constant
// };

//
// void msv(struct msvprofile *MSVPROFILE, int m, int n, int num_points,int *qdiscrete, int circular);



//  return the number of elements
int number_of_elements(char *x )
{
  FILE *infile;
  int k;
  double d;

  // first sequence
  infile = fopen(x, "r");
  if ( infile == NULL ) {  // error checking with fopen call
    printf("Unable to open file.");
    exit(1);
  }
  k = 0;
  // can calculate additional things, like square, etc, while scanning the file
  while(fscanf(infile,"%lf",&d) != EOF)
  {
      k++;
  }
  fclose(infile);
  return k;
}

// function to read data and normalize
void read_data(char *x ,  double *data, int m) // <-- pointer
{
  FILE *infile; //input file
  int i; //loop index
  double d;
  double ex , ex2 , mean, std;

  ex = ex2 = 0;

  infile = fopen(x, "r");
  i = 0;
  // can calculate additional things, like square, etc, while scanning the file
  // TODO: also calculate moving sum, moving sum of squares
  while(fscanf(infile,"%lf",&d) != EOF)
  {
      data[i] = d;
      ex += d;
      ex2 += d*d;
      i++;
  }
  fclose(infile);

  // z-normalization (global) can potentially be skipped if data already normalized
  mean = ex/m;
  std = ex2/m; // note that this is slightly different for zscore or zscore(,1) in matlab, with default being m-1?
  std = sqrt(std-mean*mean);

  // normalize here already
  for( i = 0 ; i < m ; i++ )
       data[i] = (data[i] - mean)/std;

}


// // this to fix how things are printed,
// class mystream : public std::streambuf
// {
// protected:
// virtual std::streamsize xsputn(const char *s, std::streamsize n) { mexPrintf("%.*s", n, s); return n; }
// virtual int overflow(int c=EOF) { if (c != EOF) { mexPrintf("%.1s", &c); } return 1; }
// };
// class scoped_redirect_cout
// {
// public:
//   scoped_redirect_cout() { old_buf = std::cout.rdbuf(); std::cout.rdbuf(&mout); }
//   ~scoped_redirect_cout() { std::cout.rdbuf(old_buf); }
// private:
//   mystream mout;
//   std::streambuf *old_buf;
// };



void msv(struct msvprofile *msvp, int lenQ, int lenD, double *q,double *d, int circular)
{
  // first initialize the result output_matrices (vectors)
  //
  //   MX    = -inf(lenD, 2*lenQ+1); % forward

int i;
  double *MX;
  double curMax;
  int curMaxIdx;

  int *psi;
  int *xi;
  int *oldXi;

  int N;
  int temp;

  N = 2*lenQ+2;
  int j;

	// initialize MX matrix
  MX = (double *) malloc((lenD)*(N)*sizeof(double));

  // matrix used for traceback
  psi = (int *) malloc((lenD)*(N)*sizeof(int));
  // vector used for storing k-consequtive states numbers
  xi = (int *) malloc(N*sizeof(int));
	// vector used for storing old k-consequtive states numbers
  oldXi = (int *) malloc(N*sizeof(int));

  // initialize data values
  for(int i=0;i< lenD*N;i++)
  {
    MX[i] = INF;
  }

	// initialize first row
  for(i=0; i< N;i++)
  {
    xi[i] = 1;
    psi[i] = i;
		// score is:
		// what if we use log_e?
		// loge(exp(-0.5*(val/sigma)*(val/sigma)) / (sqrt(2*M_PI) * sigma))=
		// = -0.5*(val/sigma)*(val/sigma) seems sigma just does the scaling?

		// log10(exp(-0.5*(val/sigma)*(val/sigma)) / (sqrt(2*M_PI) * sigma))
		if (i<N-2)
		{
			if (i < lenQ)
			    MX[i] = -(d[0]-q[i])*(d[0]-q[i]); // just use -ED^2 ?
			else
				MX[i] = -(d[0]-q[2*lenQ-i-1])*(d[0]-q[2*lenQ-i-1]);
		}
		else
		{
			if (i==N-1)
				MX[i] = -(d[0]*d[0]); // just the square of the value for the gap (-exp(x^2))
		}
  }
	//
  // FILE *File2;
  //
  // File2 = fopen("outMX.txt", "w+");
  // for(int k = 0 ; k < N ; k++ )
  //     fprintf(File2,"%3.15f ", MX[k]);
  // fprintf(File2,"\n");
  // fclose(File2);


//lenD
  for(i = 1 ; i < lenD; i++)
  {

	// first, we calculate N-1 state:
	// initiate current max and current max idx
    curMax = MX[i*N-1];
    curMaxIdx = N-1;
    oldXi[N-1] = xi[N-1]; // create new xi vector

    for (j = 0; j < N-2; j++) // loop through states
    {
      oldXi[j] = xi[j]; //put oldXivalus here
      if (xi[j] >= msvp->cM && MX[(i-1)*N+j]+msvp->M[1] > curMax)
      {
          curMax = MX[(i-1)*N+j]+msvp->M[1];
          curMaxIdx = j;
      }
    }
	 //
	 // if (i==31)
		// printf("curidx %d\n",  curMaxIdx );

	// printf("curmaxIdx %d\n",  curMaxIdx );
	MX[(i+1)*N-2] = curMax;
	psi[(i+1)*N-2] = curMaxIdx; // if curMaxIdx is N-1, xi non-zero

	if (curMaxIdx > N)
		xi[N-2] = oldXi[N-1];
	else
		xi[N-2] = 0;


	// now add mapping from S+1 to 1
	 if (circular == 1)
	 {
		 if ( (i > lenD-msvp->cM) || (xi[N-2] < msvp->cG) )
		    { // case match state
		        MX[i*N] =  MX[(i-1)*N+lenQ-1]+msvp->M[0];//-(d[i]-q[lenQ-1])*(d[i]-q[lenQ-1]);
		        psi[(i)*N] = lenQ-1;
				xi[0] = oldXi[lenQ-1]+1;
				MX[i*N+lenQ] =  MX[(i-1)*N+2*lenQ-1]+msvp->M[0];//+msvp->M[0]-(d[i]-q[0])*(d[i]-q[0]);
		        psi[(i)*N+lenQ] = 2*lenQ-1;
				xi[lenQ] = oldXi[2*lenQ-1]+1;
	      }
	    else // case gap state
		{
			// forward
			if (MX[(i-1)*N+lenQ-1]+msvp->M[0] > MX[(i)*N+N-2]+msvp->G[1])
	        {
				MX[i*N] =  MX[(i-1)*N+lenQ-1]+msvp->M[0];//-(d[i]-q[lenQ-1])*(d[i]-q[lenQ-1]);
				psi[(i)*N] = lenQ-1;
				xi[0] = oldXi[lenQ-1]+1;
			}
			else
			{
				MX[i*N] =  MX[(i)*N+N-2]+msvp->G[1];//-(d[i]-q[lenQ-1])*(d[i]-q[lenQ-1]);
				psi[(i)*N] = N-2;
				xi[0] = 1;
			}

			// backward
			if (MX[(i-1)*N+2*lenQ-1]+msvp->M[0] > MX[(i)*N+N-2]+msvp->G[1])
			{
				MX[i*N+lenQ] =  MX[(i-1)*N+2*lenQ-1]+msvp->M[0];//-(d[i]-q[lenQ-1])*(d[i]-q[lenQ-1]);
				psi[i*N+lenQ] = 2*lenQ-1;
				xi[lenQ] = oldXi[2*lenQ-1]+1;
			}
			else
			{
				MX[i*N+lenQ] =  MX[(i)*N+N-2]+msvp->G[1];//-(d[i]-q[lenQ-1])*(d[i]-q[lenQ-1]);
				psi[(i)*N+lenQ] = N-2;
				xi[lenQ] = 1;
			}
		}
	}
	else
	{

	   MX[i*N] =  MX[(i)*N+N-2]+msvp->G[1];//-(d[i]-q[lenQ-1])*(d[i]-q[lenQ-1]);
	   psi[(i)*N] = N-2;
	   xi[0] = 1;

	   MX[i*N+lenQ] =  MX[(i)*N+N-2]+msvp->G[1];//-(d[i]-q[lenQ-1])*(d[i]-q[lenQ-1]);
	   psi[(i)*N+lenQ] = N-2;
	   xi[lenQ] = 1;
	}

	// printf("dist %4.3f\n", - (d[i]-q[0])*(d[i]-q[0])  );
	// printf("prevVal %4.3f\n", MX[i*N]  );
	MX[i*N] = MX[i*N] - (d[i]-q[0])*(d[i]-q[0]);
	MX[i*N+lenQ] = MX[i*N+lenQ] - (d[i]-q[lenQ-1])*(d[i]-q[lenQ-1]);



	// FILE *File3;
	// File3 = fopen("outMX2.txt", "w+");
	// for(int k = N ; k < 2*N ; k++ )
	// 	fprintf(File3,"%3.15f ", MX[k]);
	// fprintf(File3,"\n");
    for (j = 1; j < lenQ; j++)
    {

      if (xi[N-2] < msvp->cG || i > lenD-msvp->cM)
      { // if only from match state to match state
        MX[i*N+j] =  MX[(i-1)*N+j-1]+msvp->M[0];
        psi[i*N+j] = j-1;
		xi[j] = oldXi[j-1]+1;
		MX[i*N+j+lenQ] =  MX[(i-1)*N+lenQ+j-1]+msvp->M[0];
		psi[i*N+j+lenQ] = lenQ+j-1;
		xi[j+lenQ] = oldXi[j-1+lenQ]+1;
      }
      else
      {

        if ( (MX[(i-1)*N+j-1]+msvp->M[0] > MX[(i)*N+N-2]+msvp->G[1]) )
          {
              MX[i*N+j] = MX[(i-1)*N+j-1]+msvp->M[0];
              psi[i*N+j] = j-1;
			  xi[j] = oldXi[j-1]+1;

          }
        else
		{
			MX[i*N+j] = MX[(i)*N+N-2]+msvp->G[1];
			psi[i*N+j] = N-2;
			xi[j] = 1;
		}
		// reverse
		if ( (MX[(i-1)*N+lenQ+j-1]+msvp->M[0] > MX[(i)*N+N-2]+msvp->G[1]) )
		  {
			  MX[i*N+lenQ+j] = MX[(i-1)*N+lenQ+j-1]+msvp->M[0];
			  psi[i*N+lenQ+j] = lenQ+j-1;
			  xi[j+lenQ] = oldXi[lenQ+j-1]+1;
		  }
		else
		{
			MX[i*N+j+lenQ] = MX[(i)*N+N-2]+msvp->G[1];
			psi[i*N+j+lenQ] = N-2;
			xi[j+lenQ] = 1;
		}
      }

	  MX[i*N+j] = MX[i*N+j] - (d[i]-q[j])*(d[i]-q[j]);
	  MX[i*N+j+lenQ] = MX[i*N+j+lenQ] - (d[i]-q[lenQ-j-1])*(d[i]-q[lenQ-j-1]);


    }

	MX[i*N+N-1]=MX[i*N+N-2]+msvp->G[0]-d[i]*d[i];
	psi[i*N+N-1] = N-2;
	xi[N-1] = xi[N-2]+1;

}

//
// FILE *FileF;
// FileF = fopen("outM.txt", "w+");
// for(int kk=0;kk<lenD;kk++)
// {
// 	for(int k = (kk)*N ; k < (kk+1)*N ; k++ )
// 		// fprintf(FileF,"%3.15f ", MX[k]);
// 		fprintf(FileF,"%4d ", psi[k]);
//
// 	fprintf(FileF,"\n");
// }
// fclose(FileF);

	// //
	// int dd = lenD-1;
	// FILE *File3;
	// File3 = fopen("outMX2.txt", "w+");
	// for(int k = (dd)*N ; k < (dd+1)*N ; k++ )
	// 	fprintf(File3,"%3.15f ", MX[k]);
	// fprintf(File3,"\n");
	// fclose(File3);
	//
	// //
	// FILE *File4;
	// File4 = fopen("outXi.txt", "w+");
	// for(int k = 0 ; k < N ; k++ )
	// 	fprintf(File4,"%4d ", xi[k]);
	// fprintf(File4,"\n");
	// fclose(File4);
	//
	// //
	// FILE *File5;
	// File5 = fopen("outPsi.txt", "w+");
	// for(int k = (dd)*N ; k < (dd+1)*N ; k++ )
	// 	fprintf(File5,"%4d ", psi[k]);
	// fprintf(File5,"\n");
	// fclose(File5);

	//  we need to do traceback too.
	MX[(lenD-1)*N+N-2] = INF; //this state won't be used
	double score = INF;
	int idx;
	idx = -1;
	for (i=0;i<N;i++)
	  if (MX[(lenD-1)*N+i] > score)
	  {
	    score =  MX[N*(lenD-1)+i];
	    idx = i+1;
	  }

	  printf("Score %f \n",  score);
	  printf("Final state %d \n",  idx);

    int newV;
	int numLines = 0;
    // FILE *File6;
  	// File6 = fopen("viterbiPath.txt", "w+");
	// fprintf(File6,"%4d %4d\n", lenD, idx);
	for (i=lenD;i>1;i--)
	{
		newV = psi[(i-1)*N+idx-1];
		printf("%4d %4d\n", i-1, newV+1);
		if (newV == N-2)
			{
				newV = psi[(i-1)*N+N-2];
				printf("%4d %4d\n", i-1, newV+1);
				numLines = numLines+1;
			}
		idx = newV+1;
		numLines= numLines+1;
	}
  	printf("\n");
  	// fclose(File6);
}

// all should go to
int main(int argc, char *argv[])
{
  int lenQ, lenD;   // len data,len query
  int i;
  double *d, *q;       /// data array and query array

  FILE *infile1; // filenames
  FILE *infile2;


  int *qdiscrete, *ddiscrete;       /// data array and query array
  int display = 1; // whether to display info

  // struct for timer calculations
  struct timeval timer_start, timer_end;

  printf("Path output...\n");

  gettimeofday(&timer_start, NULL);

  // printf ("%s \n", "Bla");

  // numbers of elements and sequences
  lenQ = number_of_elements(argv[1]);
  lenD = number_of_elements(argv[2]);

  if (display==1)
  {
  printf("Data length %d\n", lenD);
  printf("Query length %d\n", lenQ);
  }

  //
  q = (double *)malloc(sizeof(double)*lenQ);
  read_data(argv[1] , q, lenQ);
  //
  d = (double *)malloc(sizeof(double)*lenD);
  read_data(argv[2] , d ,lenD);

  // // initial parameters p_MG, p_MM, pass through as parameters (txt file) or a default
  double C_M = atof(argv[3]);  // C_M = p_MM/p_MG how many times more likely it is to jump to match state rather than gap state
  double C_G = atof(argv[4]); // C_G = p_GG/p_GM
  int circular =  atoi(argv[7]);

  // simplified msv: just match transitions, gap transitionsW
  struct msvprofile msvp;
  msvp.M = (double *) malloc((2)*sizeof(double));
  msvp.M[0] = log(C_M)-log(C_M+1); //pMM
  msvp.M[1] = -log(1+C_M); //pMG
  msvp.G = (double *) malloc((2)*sizeof(double));
  msvp.G[0] = log(C_G)-log(C_G+2*lenQ); // we make this dependent on length query
  msvp.G[1] = -log(C_G+2*lenQ);
  msvp.cM = atof(argv[5]);
  msvp.cG = atof(argv[6]);

  // //
  // if (display==1)
  // {
  printf("cM %d\n",   msvp.cM );
  // printf("C %d\n",   atoi(argv[7]) );
  printf("cg %d\n",   msvp.cG );
  printf("pMM %4.3f\n",  msvp.M[0] );
  printf("pMG %4.3f\n",   msvp.M[1]);
  printf("pGG %4.3f\n",  msvp.G[0]  );
  printf("pGM %4.3f\n",   msvp.G[1] );


  // }

  msv(&msvp, lenQ, lenD, q ,d ,circular);

  gettimeofday(&timer_end, NULL);

  return 0;
}




//
// /* The gateway function that replaces the "main".
//  *plhs[] - the array of output values
//  *prhs[] - the array of input values*/
//
// void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
// {
//     /// output location
//     double *y;
//     /// output score
//     double *s;
//
// 	  //  double R;
//     // /*Variable declarations as in C*/
//     // ///  char Data_File, Query_File;
//     // int M;
//
//
//     ///int L; /// length of the overlap
//
//   int buflen0, status0;
//   int buflen1, status1;
//
// 	char *input_buf0, *output_buf0;
// 	char *input_buf1, *output_buf1;
//
//  ///
//  ///   Query_File = mxGetString(prhs[1]);
//
//
//     /* Check for proper number of arguments. */
//     if (nrhs != 2) {
//         mexErrMsgTxt("Five inputs required.");
//   	}
//     else if (nlhs > 2) {
//         mexErrMsgTxt("Too many output arguments");
//     }
//
//     // M = mxGetScalar(prhs[2]); /// overlap length
//     // R = mxGetScalar(prhs[3]); /// Sakoe-Chiba width
//
//       /* Get the length of the input string. */
//     buflen0 = (mxGetM(prhs[0]) * mxGetN(prhs[0])) + 1;
//     /* Allocate memory for input and output strings. */
//   	input_buf0 =(char *) mxCalloc(buflen0, sizeof(char));
//
//     /* Copy the string data from prhs[0] into a C string
//     * input_buf. */
//    	status0 = mxGetString(prhs[0], input_buf0, buflen0);
//
// 	/* Get the length of the input string. */
//     buflen1 = (mxGetM(prhs[1]) * mxGetN(prhs[1])) + 1;
//     /* Allocate memory for input and output strings. */
// 	input_buf1 =(char *) mxCalloc(buflen1, sizeof(char));
//
//     /* Copy the string data from prhs[0] into a C string
//     * input_buf. */
//    	status1 = mxGetString(prhs[1], input_buf1, buflen1);
//
// //     /*
//     mexPrintf("Number of inputs:  %d\n", nrhs);
//     mexPrintf("Number of outputs: %d\n", nlhs);
// 	// mexPrintf("M: %d\n", M);
//   //   mexPrintf("R: %4.3f\n", R);
//     mexPrintf("Data_File length: %d\n", mxGetM(prhs[1]));
//     mexPrintf("Query_File length: %d\n", mxGetM(prhs[0]));
// //     */
//
// 	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
// 	y = mxGetPr(plhs[0]);
//
//   plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);
// 	s = mxGetPr(plhs[1]);
//
//
//     // cpp_dtw(input_buf0, input_buf1, M, R, y,s);
//
//
// }
