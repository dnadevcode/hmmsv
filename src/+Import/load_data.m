function [data] = load_data(input,sets,name)
    % load data for HMMSV
    %
    %
    dataname = fullfile(sets.output.outfold,sets.timestamp,strcat(name,'_input_files.txt'));
    fd = fopen(dataname,'w');
    
    
    switch input.type
%         case 'data'
%             data{i}.barcode =  data{i}.barcode(data{i}.bitmask);
%             data{i}.bitmask = ones(1,length( data{i}.barcode)); % now it's no nans

        case 'consensus'
            for i=1:length(input.filenames)
                fprintf(fd,'%s\n', fullfile(input.filefolds{i},input.filenames{i}));
            end
            % load consensus from files
            import Import.load_cons;
            [data] = load_cons(input.filenames,input.filefolds,sets.output.outfold);
            
        case 'theory'
            for i=1:length(input.filenames)
                fprintf(fd,'%s\n', fullfile(input.filefolds{i},input.filenames{i}));
            end
            % load consensus from files
            import Import.load_theory;
            [data] = load_theory(input.filenames,input.filefolds,sets.output.outfold);
        case 'synthetic'
            for i=1:length(input.filenames)
                fprintf(fd,'%s\n', fullfile(input.filefolds{i},input.filenames{i}));
            end
            import Import.load_synthetic;
            [data] = load_synthetic(input.filenames,input.filefolds,sets.output.outfold);
    end

    
    fclose(fd);

%     
%     if nargin < 4
%         % default data inputs
%         switch type
%             case 'consensus'
%                 sets.fold = '/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/';
%                 % temp change
% %                 sets.fold = '/home/albyback/Desktop/issueplottingaftercbtv4_0_0/data/reissueplottingaftercbtv4_0_0/';
%             case 'kymographs'
%                 sets.fold = '/home/albyback/git/sv/data/runDataMBIO/';
%                 addpath(genpath(sets.fold));
%             case 'database'
%                 sets.fold = '/home/albyback/git/sv/data/runDataDatabase/';
%             case 'theory' 
%                 sets.fold = '/home/albyback/git/sv/data/runData/';
%                 theories = '/home/albyback/git/sv/data/runData/*.fasta';
% 
% %                 [tifsName,lambdasName] = create_database_files(source,setsF,resultFold);
%             otherwise
%         end
%     else
%         sets.fold = fold;
%     end
    
%     sets.outfold = outfold;
%           sets.outfold = outfold;

%     mkdir(outfold);
%     
%     import import.load_mbio;
%     import import.load_kymograph;
%     % open file to save txts, could have a different name with a timestamp
%     % i.e.
%     fd = fopen(fullfile(outfold,'consensus_mat_files.txt'),'w');
%     
%     switch type
%         case 'consensus'
% %             sets.fold = '/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/';
%             
%             % read data / would rather load data from txt file..
%             listing = dir(strcat(sets.fold,'*.mat') );
%             uniqueNames = arrayfun(@(x) x.name, listing,'UniformOutput',false);
%             uniqueFolds = arrayfun(@(x) x.folder, listing,'UniformOutput',false);
%             for i=1:length(uniqueNames)
%                 fprintf(fd,'%s\n', fullfile(uniqueFolds{i},uniqueNames{i}));
%             end
%             [data] = load_cons(uniqueNames,uniqueFolds,outfold);
% 
% %             [sets,uniqueNames,uniqueFolds] = load_mbio(sets);
%         case 'kymographs'
%             % the folders with kymographs have to have a specific structure
%             % for us to be able to extract all of them
%             [uniqueNames,lambdasName] = create_database_files(sets.fold,sets,outfold);
%             uniqueFolds = sets.fold;
%             for i=1:length(uniqueNames)
%                 for j=1:length( uniqueNames{i})
%                     if iscell( uniqueNames{i})
%                         fprintf(fd,'%s\n', uniqueNames{i}{j});
%                     else
%                         fprintf(fd,'%s\n', uniqueNames{i});
%                     end
%                 end
%             end
%             [data] = load_kymograph(horzcat(uniqueNames{:}),lambdasName,sets);
%         case 'database'
%             [uniqueNames,lambdasName] = create_database_files(sets.fold,sets,outfold);
%             uniqueFolds = sets.fold;
%             for i=1:length(uniqueNames)
%                 fprintf(fd,'%s\n', uniqueNames{i});
%             end
%             [data] = load_kymograph(uniqueNames,lambdasName,sets);
%         case 'theory'
%             [uniqueNames,lambdasName] = create_database_files(sets.fold,sets,outfold);
%             uniqueFolds = sets.fold;
%             for i=1:length(uniqueNames)
%                 fprintf(fd,'%s\n', uniqueNames{i});
%             end
%             [data] = load_kymograph(uniqueNames,lambdasName,sets);
% %             uniqueNames= [uniqueNames 
%             % import settings
% %             import CBT.Hca.Import.import_hca_settings;
% %             [sets2] = import_hca_settings(sets.txt);
%             st = length(data)+1;
%             [t,theoriesTxt,theoryStruct, sets,theoryGen] = HCA_theory_parallel(theories,data{1}.bpnm,sets.txt2);
%            data = [data theoryStruct];
%             for i=1:length(theoryStruct)
%                 data{st}.barcode = theoryGen.theoryBarcodes{i};
%                 data{st}.fname = theoryStruct{i}.filename;
%                 data{st}.circ = ~sets.theoryGen.isLinearTF;
%                 if  data{st}.circ
%                     data{st}.bitmask = ones(1,length(data{st}.barcode ));
%                 else % this case add some zeros
%                     data{st}.bitmask = ones(1,length(data{st}.barcode ));
%                     data{st}.bitmask(1:5) = 0;
%                     data{st}.bitmask(end-4:end) = 0;
%                     data{st}.bpnm = data{1}.bpnm;
%                 end
%                 st = st+1;
%             end
%             % sets.resultsDir = 'out/';
%             % mkdir(sets.resultsDir);
% 
%             % double check puuh_theory.txt
%             % generate theory
% %             sets.theoryGen.meanBpExt_nm = bpnmTheory;
% %             sets.theoryGen.pixelWidth_nm = 159.2;
% %             [t,theoriesTxt,theoryStruct, sets,theoryGen] = HCA_theory_parallel(theories,bpnm,'puuh_theory.txt');
% 
% 
%         case 'synthetic'
%             % here we create synthetic data
%             % Options: linear-linear, linear-circular, circular-linear, and
%             % circular-circular
%             % if we want linear
%             
%             %generates a listoflinear barcodes
%             svTypes = sets.svType*ones(1,sets.numSamples);
%             len1 = sets.length1;
%             len2 = sets.length2;
%             circ = [sets.circ sets.circ];
%             sigma = sets.kernelsigma;
%             noise = 1-sets.pccScore;
%             
%             import Rand.gen_synthetic_sv;
%             [bar1,bar2,matchTable] = gen_synthetic_sv(len1,len2, svTypes, circ, sigma,noise);
%             uniqueNames =[];
%             uniqueFolds = [];
%             
%             data = cell(1,2*length(bar1));
%             for i=1:length(bar1);
%                 %                 % save DATA
%                 rS = fullfile(outfold,strcat([ num2str(i) '_query_seq.txt']));
%                 fid = fopen(rS,'w');
% 
%                 fprintf(fid, '%5.4f ', bar1{i}  );
%                 fprintf(fid, '\n');
%                 fclose(fid);
%                 data{2*i-1}.fname = rS;
% 
%                 % save QUERY (this includes the variation)
%                 rS = fullfile(outfold, strcat([ num2str(i) '_data_seq.txt']));
%                 fid = fopen(rS,'w');
%                 if sets.circ % save txt as circular..
%                     fprintf(fid, '%5.4f ',[bar2{i} bar2{i}(1:sets.c-1)]   );
%                 else
%                     fprintf(fid, '%5.4f ',bar2{i}   );
%                 end
%                 fprintf(fid, '\n');
%                 fclose(fid);
%                 data{2*i}.fname = rS;
% 
% 
%                 data{2*i-1}.barcode = bar1{i};
%                 data{2*i}.barcode = bar2{i};
%                 data{2*i-1}.matchTable =matchTable{i};
% %                 data{2*i}.fname 
%                 data{2*i-1}.circ = circ(1);
%                 data{2*i}.circ = circ(2);
%                 data{2*i}.bitmask = ones(1,length(data{2*i}.barcode ));
%                 data{2*i-1}.bitmask = ones(1,length(data{2*i-1}.barcode ));
%                 try
%                 data{2*i-1}.bpnm = sets.bpnm; 
%                 data{2*i}.bpnm = sets.bpnm;
%                 end
% 
%             end
% %             
% %             import Rand.generate_linear_sv;
% %             [bar1,bar2,matchTable,lengths]  = arrayfun(@(x) generate_linear_sv(len1, lenVar, x,sets),sets.svList,'UniformOutput',false);
%             
% %             idx=5;
% %             res.bar1 = bar1{idx};
% %             res.bar2 = bar2{idx};
% %             res.matchTable = matchTable{idx};
% %             res.pass = ones(1,size(matchTable{idx},1));
% % 
% %             sets.fold = outfold;
% % %             plot full result
% %             import Plot.plot_sv_full;
% %             cellfun(@(x) plot_sv_full( x,sets,'test111.eps',2,0,0.5),{res},'UniformOutput',false);
% 
%             %
%             % test - all pccs should be equal one
%             
%             % these can be saved as .tifs
%             
%             % or
% %             import Rand.generate_one_type_sv;
% %             [randStruct] = generate_one_type_sv(sets,vals(i));
% 
% 
%         otherwise
%             
%     end
%     
%     
%     
%     
% [file,fold] = uigetfile('*.mat','select query file','MultiSelect','on', sets.fold);

% listing = dir(strcat(sets.fold,'*.mat') );
% names = arrayfun(@(x) x.name, listing,'UniformOutput',false);
% folds = listing(1).folder;
% 
% sets.fold = '/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/';
% % [file,fold] = uigetfile('*.mat','select query file','MultiSelect','on', sets.fold);
% 
% listing = dir(strcat(sets.fold,'*.mat') );
% names = arrayfun(@(x) x.name, listing,'UniformOutput',false);
% folds = listing(1).folder;
% 
% 
% 
%     for i=1:length(fileNames)
%         
%         
%     end

end

