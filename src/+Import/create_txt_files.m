function [queries] = create_txt_files(foldToSaveData,bar1,bar2withNoise,circ,c)
    % save barcodes
    queries = cell(1,length(bar1));
    for i=1:length(bar1)
        query = fullfile(foldToSaveData,strcat(['bar_query_' num2str(i) '_.txt']));
        queries{i} = query;
        fd =fopen(query,'w');   fprintf(fd,"%16.15f ", bar1{i});    fclose(fd);
        data = fullfile(foldToSaveData, strcat(['bar_data_' num2str(i) '_.txt']));
        fd =fopen(data,'w');   
        if circ
            fprintf(fd,"%16.15f ", [bar2withNoise{i} bar2withNoise{i}(1:c-1)]);   
        else
            fprintf(fd,"%16.15f ", bar2withNoise{i}); 
        end
         fclose(fd);
    end
    
end

