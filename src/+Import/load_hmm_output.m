function [match_table,pathV] = load_hmm_output(nameQuery)

    % todo: check if the file contains alignmen table or viterbi path
    file = strrep(nameQuery,'query','query_output');
    
    % first line
    fid = fopen(file);
    firstLine = strsplit(fgetl(fid), ';');
    fclose(fid);
    if ~isequal(firstLine{1}(1:4),'Path')
        % load data from a single hmm file
        A = importdata(file,' ',2);
        try
            rs.matchTable = A.data;
        catch
            rs.matchTable = [];
        end

            % note that we should do this for MATLAB too. Don't convert!
            match_table = rs.matchTable; %convert_matchtable(rs);
        if ~isempty(match_table)
            match_table(:,end) = 1:size(match_table,1);
        end
    else
        A = importdata(file,' ',11);
        pathV = A.data;
        lenQ = strsplit(A.textdata{3},' ');
        lenQ = str2double(lenQ{end});
%        lenD = strsplit(A.textdata{3},' ');
%         lenD = str2double(lenQ{end});
        
        [match_table] = create_t(pathV,lenQ);
        if ~isempty(match_table)
            match_table = flipud(match_table);
            match_table(:,end+1) = 1:size(match_table,1);
            newValR4 =  2*lenQ-match_table(match_table(:,3)>lenQ,3)+1; % since 2*lenQ-1 is actually 1!
            newValR3 = 2*lenQ-match_table(match_table(:,4)>lenQ,4)+1;
            match_table(match_table(:,3)>lenQ,3) =  newValR3;
            match_table(match_table(:,4)>lenQ,4) =  newValR4;
        end
        
        % now check if lengths are too long.
        for i=1:size(match_table,1)
            % if > lenQ, then need to reduce just to lenQ
            if match_table(i,2)-match_table(i,1)+1>lenQ
                match_table(i,2) = match_table(i,1)+lenQ-1;
                if match_table(i,5)==1
                     match_table(i,4) = mod(match_table(i,3)-1-1,lenQ)+1;
                else
                    match_table(i,4) = mod(match_table(i,3),lenQ)+1;
                end
            end
        end

    end
    
    % we compare data vs query. If data is 500 ox long, query is 500 px
    % long  1   542     1    42     1
%     if size(match_table,1)==1
    

% 
%    % This works both for circular and linear barcodes
%    import functions.table_to_bar;
%    [barqf, bardf,bit1,bit2] = arrayfun(@(x) table_to_bar( match_table(x,:),y,bar2),1:size( match_table,1),'UniformOutput',false);
% 
% %         % a bit hard to compare with the matlab version since that
% %         % one at the moment reports the PCC for discrete data
%     comparisonStruct.pcc_table = cellfun(@(x,u) zscore(x,1)*zscore(u',1)/length(x),barqf,bardf);
% %         comparisonStruct{j}.aligment_table = match_table;
% %         comparisonStruct{j}.fullTable = fullTable;
%     comparisonStruct.lengths = cellfun(@(x) length(x),barqf);
%     try
%         score = str2double(strrep(A.textdata{1},'Score',''));
%     catch
%          score  = -inf;
%     end
%     
    
%     scoreCombined  =  cellfun(@(x) sum(x.pcc_table.*x.lengths)/sum(x.lengths),comparisonStruct);
%     [bestpcc,idx] =  max(scoreCombined);
%     bestSt = sets.analysis.stretchFactors(idx);
%     stretchStruct.bestSt = bestSt;
%     stretchStruct.stretchFactors = sets.analysis.stretchFactors;
%     stretchStruct.idx = idx;
%     
%     % now remove
% 	system(strcat(['rm ' nameData]));
%     system(strcat(['rm ' fullfile(sets.output.outfold,sets.timestamp, strcat([num2str(name) '*' '_query.txt']))]));
%     system(strcat(['rm ' fullfile(sets.output.outfold,sets.timestamp, strcat([num2str(name) '*' '_query_output.txt']))]));

    
end

