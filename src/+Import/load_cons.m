function [data] = load_cons(uniqueNames,uniqueFold,outfold)
    % Args:
    %   fold,sample1
    % Returns:
    %   A, B
    %
    % all consensuses
    % fold = '*.mat';
%     files = dir(fold);
    if nargin < 3
        outfold = 'outdata';
    end

    % take the first sample
    % sample1 = {'P11K0','P11K22'}; 
%     sample1 = {'P6K0','P6K25'}; 
% 
%     settings = 'sample1_settings.txt';

    % camera res - unknown (. nm/bp stretch for each sample - unknown. Hence
    % convertion px to bp - unknown. Henceforth need stretching to get the best!

    % load data
    data = cell(1,length(uniqueNames));
    for i=1:length(uniqueNames)
        data{i} = load(fullfile(uniqueFold{i},uniqueNames{i}));
        
        % save barcode in a txt file in output folder
        fname = fullfile(outfold,strcat([ uniqueNames{i} '.txt']));
        fileID = fopen(fname,'w'); % only print the barcode without bitmask to data file
        fprintf(fileID,'%2.16f ',data{i}.clusterConsensusData.barcode(data{i}.clusterConsensusData.bitmask));
        fclose(fileID);
        data{i}.fname = fname;
        data{i}.barcode = data{i}.clusterConsensusData.barcode;
        data{i}.bitmask = data{i}.clusterConsensusData.bitmask;
        data{i}.indexWeights = data{i}.clusterConsensusData.indexWeights; % index weights, i.e. how many barcodes averaged at each position
        try
        data{i}.alignedBarcodes = data{i}.clusterConsensusData.clusterResultStruct.alignedBarcodes; % save also aligned barcodes in case these could be used
        catch
        end
        % whether barcode is circular
        data{i}.circ = isempty(find(data{i}.clusterConsensusData.bitmask==0));
        try
             data{i}.bpnm = data{i}.clusterConsensusData.bpnm;
        catch
        end
        data{i}.barcode =  data{i}.barcode(data{i}.bitmask);
        data{i}.bitmask = ones(1,length( data{i}.barcode)); % now it's no nans

        % should we resize to larger size so that rescaling would have smaller affect?
        %         data{i}.barcode = data{i}.clusterConsensusData.barcode;
        %         data{i}.bitmask = data{i}.clusterConsensusData.bitmask;
    end

end

