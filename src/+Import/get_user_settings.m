function [sets] = get_user_settings(sets)
    % get_user_settings
    %
    % This function asks for all the required settings for the generation
    % of the results at this point.
    %
	%     Args:
    %         sets (struct): Input settings to the method
    % 
    %     Returns:
    %         sets: Return structure
    
    

    if ~sets.askfordata
        % first queries
        try 
            fid = fopen(sets.query.dataFile); 
            dataNames = textscan(fid,'%s','delimiter','\n'); fclose(fid);
            for i=1:length(dataNames{1})
                [FILEPATH,NAME,EXT] = fileparts(dataNames{1}{i});
                sets.query.filenames{i} = strcat(NAME,EXT);
                sets.query.filefolds{i} = FILEPATH;
            end
            fid = fopen(sets.data.dataFile); 
            dataNames = textscan(fid,'%s','delimiter','\n'); fclose(fid);
            for i=1:length(dataNames{1})
                [FILEPATH,NAME,EXT] = fileparts(dataNames{1}{i});
                sets.data.filenames{i} = strcat(NAME,EXT);
                sets.data.filefolds{i} = FILEPATH;
            end
        catch
            sets.askfordata = 1;
        end
    end
    
       
    if sets.askfordata 
        % loads figure window
        import Fancy.UI.Templates.create_figure_window;
        [hMenuParent, tsHCA] = create_figure_window('HMMSV data select tool','HMMSV');

        import Fancy.UI.Templates.create_import_tab;
        cache = create_import_tab(hMenuParent,tsHCA,'Data');
        uiwait(gcf);  
        
        dd = cache('selectedItems');
        sets.filenames{i} = dd(1:end/2);
        sets.filefolds{i} = dd((end/2+1):end);
        delete(hMenuParent);
    end
    
    % ask for other settings
        
    if sets.askforsets
    
        prompt = {'Number of time frames (all time frames by default)','Alignment method (1 is nralign, 2 is ssdalign)', 'Filter the barcodes', 'Add consensus','Non-default bitmask settings', 'Non-defaut edge detection settings', 'Skip edge detection', 'Generate random fragments cut-outs','Generate independent subfragments for barcodes','Comparison Method'};
        title = 'HMMSV settings';
        dims = [1 35];
        definput = {'0','1','0','0','0','0','0','0','0','mass_pcc'};
        answer = inputdlg(prompt,title,dims,definput);
        

        if sets.output.askforoutputdir
            sets.output.outfold = strcat([uigetdir(pwd,'Choose a folder where you want to save the output') '/']);
        end
    end

    % set up output dir path
    mkdir(sets.output.outfold);
    mkdir(sets.output.outfold,sets.timestamp);
 
                
end

