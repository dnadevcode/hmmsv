function [data] = load_theory(uniqueNames,uniqueFold,outfold)
    % Args:
    %   fold,sample1
    % Returns:
    %   A, B
    %
    % all consensuses
    % fold = '*.mat';
%     files = dir(fold);
    if nargin < 3
        outfold = 'outdata';
    end

    data = cell(1,length(uniqueNames));
   
    % load data
    data = cell(1,length(uniqueNames));
    for i=1:length(uniqueNames)
        bar = importdata(fullfile(uniqueFold{i},uniqueNames{i}));
        bit = importdata(fullfile(uniqueFold{i},strrep(uniqueNames{i},'_barcode','_bitmask')))<20;

        % save barcode in a txt file in output folder
        fname = fullfile(outfold,strcat([ uniqueNames{i} '.txt']));
        fileID = fopen(fname,'w'); % only print the barcode without bitmask to data file
        fprintf(fileID,'%2.16f ',bar(bit));
        fclose(fileID);
        data{i}.fname = fname;
        data{i}.barcode = bar(bit);
        data{i}.bitmask = bit;
        data{i}.indexWeights = bit; % index weights, i.e. how many barcodes averaged at each position
        data{i}.alignedBarcodes = []; % save also aligned barcodes in case these could be used
        % whether barcode is circular
        data{i}.circ = isempty(find(bit==0));
        [dat,file,parts] = fileparts(uniqueNames{i});
        splitname = strsplit(file,'_');
        try
             data{i}.bpnm = str2double(splitname{end-4});
        catch
        end

        % should we resize to larger size so that rescaling would have smaller affect?
        %         data{i}.barcode = data{i}.clusterConsensusData.barcode;
        %         data{i}.bitmask = data{i}.clusterConsensusData.bitmask;
    end

end

