function [data] = load_synthetic(uniqueNames,uniqueFold,outfold)
    % Args:
    %   uniqueNames
    %   uniqueFold
    %   outfold
    % Returns:
    %  	data
    %
    % all synthetic files
    % if doesnt exists, should generate synthetic files instead
%     files = dir(fold);
    if nargin < 3
        outfold = 'outdata';
    end

    % take the first sample
    % sample1 = {'P11K0','P11K22'}; 
%     sample1 = {'P6K0','P6K25'}; 
% 
%     settings = 'sample1_settings.txt';

    % camera res - unknown (. nm/bp stretch for each sample - unknown. Hence
    % convertion px to bp - unknown. Henceforth need stretching to get the best!

    % load data
    data = cell(1,length(uniqueNames));
    for i=1:length(uniqueNames)
        fname = fullfile(uniqueFold{i},uniqueNames{i});
        barcode = importdata(fname);
        
  
        data{i}.fname = fname;
        data{i}.barcode = barcode;
        data{i}.bitmask = ones(1,length(barcode)); % bitmask assumed to be ones
        data{i}.indexWeights =  ones(1,length(barcode)); % index weights, i.e. how many barcodes averaged at each position
        % whether barcode is circular
        vals = strsplit(fname,'_');
        data{i}.circ = eval(vals{end-1});
%          = isempty(find(data{i}.clusterConsensusData.bitmask==0));


    end

end

