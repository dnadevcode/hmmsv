function create_txt_file(settings,filenames)
    % create_txt_file - creates a txt file with a given name with given
    % files
    
    %   Args: 
    %       settings - contians info about where the txt file should be saved
    %       filenames - filenames to be saved in the txt file
    fd =fopen(settings.dataFile,'w');
    for i=1:length(filenames)
        fprintf(fd,'%s\n', filenames{i});
    end
    fclose(fd);


end

