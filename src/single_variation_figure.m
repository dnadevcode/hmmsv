function [query,data,tableData,res,settingsHMM] = single_variation_figure(pval,svtype)

rng(1,'twister');

sets = ini2struct( 'default_synthetic_settings.txt');

if nargin >=2
   sets.svType = svtype;
end

% first generate some synthetic data..
import Rand.gen_synthetic_sv;
[bar1,bar2,matchTable] = gen_synthetic_sv(sets.len1,sets.len2, sets.svType, sets.circ, sets.sigma,sets.noise);

% % maybe plot this to check?
% import Plot.plot_sv;
% plot_sv( bar1, bar2,matchTable,sets,'Fig1.eps');

settingsHMM = ini2struct( 'hmmsv_default_settings.txt');
% set up output dir path
mkdir(settingsHMM.output.outfold);

if nargin>=1
%     settingsHMM.pval.pvalFile = pval;
    splitName = strsplit(pval,'_');
    splitName{end-5} = num2str(length(bar2{1}));
    splitName{end-6} = num2str(length(bar1{1}));
    settingsHMM.pval.pvalFile = strjoin(splitName,'_');
else
    % pregenerate
end

query = {fullfile(settingsHMM.output.outfold,strcat(['bar1_query_' num2str( sets.circ) '_.txt']))};
fd =fopen(query{1},'w');   fprintf(fd,"%16.15f ", bar1{1});    fclose(fd);
data = {fullfile(settingsHMM.output.outfold, strcat(['bar1_data_' num2str( sets.circ) '_.txt']))};
fd =fopen(data{1},'w');   fprintf(fd,"%16.15f ", bar2{1});    fclose(fd);
tableData = fullfile(settingsHMM.output.outfold, 'bar1_matchTable.txt');
fd =fopen(tableData,'w');   fprintf(fd,"%5d ", matchTable{1});    fclose(fd);
        
% now save bar1 bar2 matchTable and settings into txt files
% Example how to run consensus vs consensus for hmmsv. Single query
% and single data

% query = repmat({'/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/P1K0.mat'},3,1);
% data = {'/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/P3K0.mat',...
%     '/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/P5K0.mat',...
%     '/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/P8K0.mat'};

settingsHMM.query.type = 'synthetic';
settingsHMM.query.dataFile = 'src/settings/query.txt';

settingsHMM.data.type = 'synthetic';
settingsHMM.data.dataFile = 'src/settings/data.txt';

import Import.create_txt_file;
create_txt_file(settingsHMM.query,query);
create_txt_file(settingsHMM.data,data);

% figure name
settingsHMM.output.figsfold = 'output/';
settingsHMM.name = strcat(['test11' num2str(svtype) '.eps']);

% for p-value pregeneration, here we re-use the same file from determine
% noise dependence. We also allow for re-scaling here, should we take that
% into account when computing p-value?
settingsHMM.estlength = length(bar2{1})/2;
[res,settingsHMM] = hmmsv_run(settingsHMM);

end

