function [tpr] = compute_tpr(mergedTable,matchTable, len1, len2 , settingsHMM)
    % Compute true positive rates given mergedTable and true table

%      tpr= zeros(1,length(mergedTable));
%     for idx=1:length(mergedTable)
        % do for the mergedTable:
        import functions.table_to_vec;

        % these might be approximate matchings
        % here we generate the vectors of matching pixels
        [vecb1,cartesianCoords1b] = arrayfun(@(x) table_to_vec(mergedTable(x,1:2),len2,1),1:size(mergedTable,1),'un',false);
        [vecb2,cartesianCoordsb2] = arrayfun(@(x) table_to_vec(mergedTable(x,3:4),len1,mergedTable(x,5)),1:size(mergedTable,1),'un',false);

        % these will be pixel to pixel matches
        [vecr2,cartesianCoordsr2] = arrayfun(@(x) table_to_vec(matchTable(x,1:2),len1,1),1:size(matchTable,1),'un',false);
        [vecr1,cartesianCoordsr1] = arrayfun(@(x) table_to_vec(matchTable(x,3:4),len2,matchTable(x,5)),1:size(matchTable,1),'un',false);

%         sets.st=1;
    %     figure,plot(cartesianCoordsr2{1},'x')
    %     hold on
    %     plot(cartesianCoordsr1{1},'x')
    %     figure,plot(cell2mat(vecb2),cell2mat(vecb1),'x')
    %     hold on
    %     plot(cell2mat(vecr2),cell2mat(vecr1),'o')
    %     
    % %     so we have direct relationship between cell2mat(vecr2) and cell2mat(vecr1)
    %     vecMatches = zeros(2,length(bar2withNoise{idx}));
    %     rel = [cell2mat(vecr1); cell2mat(vecr1)];
        sizeM = size(mergedTable,1);
%         settingsHMM.comparison.st = 1;

        % HOW TO PROPERLY COMPUTE POSITIVE RATE?
        
        % we have vecb2 and vecb1 unequal lengths.
        % smaller than it should be?
        import Stats.true_positives_approx;
        tpr = true_positives_approx(cell2mat(vecr2),cell2mat(vecr1),vecb2,vecb1,len1,len2,settingsHMM,sizeM);
%         tpr];
%     end
end

