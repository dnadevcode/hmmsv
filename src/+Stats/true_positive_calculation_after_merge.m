function [sensitivity,specificity] = true_positive_calculation_after_merge(true_table, match_table,len1,len2,sets)
    % true_positive_calculation
    %
    %   This is a flexible function, allow us to compute the true positives
    %   with error margins, and able to separate into linear/circular calse
    % Also, this includes possibility of computing true positives after
    % closing gaps.
    
    % computes the ration of correctly matching pixels from the intervals
    % in the match table
    %
    %
    %   Args:
    %       true_table, match_table
    %   Returns:
    %       TP, FN
    %
    %
    %   Note: We asssume 1-1 and no overlaps between matches
    %
    %
    %   Example:
    %     true_table =
    % 
    %      1   114     1   114
    %    115   500   165   550
    % 
    % 
    %     match_table =
    % 
    %      1   114     1   114
    %    120   500   170   550
    %
    %
    % have to be a bit careful about how NaN's are treated since they could
    % mess up the rates.
    %
    %
    % Check this a bit more careful, and then write down to the draft. Now,
    % it can be that both true_table values and match_tables values are
    % looping circularly... Maybe smartest way is to do this interval
    % based true positives? Also it has to be both for the main segment and
    % for the variation segment.
 
    % First, compute true positives
    %
    % Now, true_table has values,'qstart':'qend', 
    % values on reference 'sstart': 'send' 
    % 
    % so the total number of tp+fn is the number of elements in this
    % true table
    % now we want to classify each value of this as true positive/false
    % negative.
    
    % match table has intervals matching query to reference. 
    % if there is a match table value that matches pixel of the query to
    % pixel of the reference to within +-1/2 px, we say that this pixel is
    % a true positive. if it matches somewhere further away, or does not
    % match at all, we say that it is a false negative.
    % how to compute this efficiently? Below is a lot of trial code of
    % computing true positive/false negatives. We need to take into account
    % that in the true table, both query and data can be circularly shifted
    % (bot actually we don't allow query to be circularly shifted in
    % running the HMM.
    
    % begin from matchtable. E); %ach index query matched to unique index of
    % data
    % true table: query first, data second
    % match table: data first, query second (since we allow circleshift in
    % query
    
    
    if nargin < 5
        st = 0;
    else
        st = sets.st;
    end
    
    % first two rows are query, so it's length is len2
%     trueVec = nan(1,len1);
%     matchVec = nan(1,len1);
%     
    % create valuesToMatch, which store the points matched using the
    % algorithm
    valuesToMatch = cell(1,len1);
    valuesToMatchBinary = cell(1,len1);
%  valuesToMatchFalseBinary = cell(1,len2);
    matchValuesMatchmatrix  = zeros(len1,len2);

    % all values of len1
    allValues = ones(1,len2);
    % stores indices of false postives
%     falsePositiveIdx =zeros(1,len2);

    % query elements might repeat, but data elements assigned are unique.
    % data assigned is in vals2
    % valuesToMatch has as an index query value.
    for k=1:size(match_table,1)  
        if match_table(k,5) == 2
            vals2 = match_table(k,3):-1:match_table(k,4);
            vals1 = match_table(k,1):match_table(k,2);
        else
            vals2 = match_table(k,3):match_table(k,4);
            vals1 = match_table(k,1):match_table(k,2);
        end
        
        % cut of trailing pixels at the end of the match
        maxL = min(length(vals2),length(vals1));
        vals2 = vals2(1:maxL);
        vals1 = vals1(1:maxL);
        
        for jj=1:length(vals1)
            valuesToMatch{vals1(jj)} = [valuesToMatch{vals1(jj)} vals2(jj)];
            valuesToMatchBinary{vals1(jj)} = [valuesToMatchBinary{vals1(jj)} 0];
            allValues(vals2(jj)) = 0;
            matchValuesMatchmatrix(vals1(jj),vals2(jj)) = 1;
        end
    end
    
    valuesToMatchFalseBinary = valuesToMatchBinary;
%     tic
    % create trueValuesToMatch, which store the points matched using the
    % algorithm
    trueValuesToMatch = cell(1,len1);
    for k=1:size(true_table,1)  
        if true_table(k,5) == 2
            vals2 = true_table(k,3):-1:true_table(k,4);
            vals1 = true_table(k,1):true_table(k,2);
        else
            vals2 = true_table(k,3):true_table(k,4);
            vals1 = true_table(k,1):true_table(k,2);
        end
        
        for jj=1:length(vals1)
            trueValuesToMatch{vals1(jj)} = [trueValuesToMatch{vals1(jj)} vals2(jj)];
        end
    end
%     toc
%     tic
%     % show this as a matrix
%     trueValuesMatchmatrix = zeros(len1,len2);
%     for k=1:size(true_table,1)  
%         if true_table(k,5) == 2
%             vals2 = true_table(k,3):-1:true_table(k,4);
%             vals1 = true_table(k,1):true_table(k,2);
%         else
%             vals2 = true_table(k,3):true_table(k,4);
%             vals1 = true_table(k,1):true_table(k,2);
%         end
%         for jj=1:length(vals1)
%             trueValuesMatchmatrix(vals1(jj),vals2(jj)) = 1;
%         end
%     end
%     f = figure;
% hold on
%     imshow(~trueValuesMatchmatrix)
%     axis on;
%     axis('xy') 
%     toc
% 
%     matchValuesMatchmatrix  = zeros(len1,len2);
%     for k=1:size(match_table,1)  
%         if match_table(k,5) == 2
%             vals2 = match_table(k,3):-1:match_table(k,4);
%             vals1 = match_table(k,1):match_table(k,2);
%         else
%             vals2 = match_table(k,3):match_table(k,4);
%             vals1 = match_table(k,1):match_table(k,2);
%         end
%         
%         for jj=1:length(vals1)
%             matchValuesMatchmatrix(vals1(jj),vals2(jj)) = 1;
%         end
%     end
    
%         f = figure;
%     imshow(~matchValuesMatchmatrix)
%     axis on;
%     axis('xy') 
%     toc
% 
%     figure,imshowpair(~matchValuesMatchmatrix,~trueValuesMatchmatrix)
    
    % now go through all trueValuesToMatch and see how they are matched
    numMatch = 0;
    numDismatch = 0;
    for k=1:len1 % length of query
        % where was  supposed to match. Might be multiple.. (say 1 and 40)
        positionsToMatchTo = trueValuesToMatch{k};
        
        % where was matched really
        positionsMatched =  valuesToMatch{k};
        
        if isempty(positionsToMatchTo)
            % case when there are no positives
            % then all positionsMatched are treated as false positives
            valuesToMatchFalseBinary{k}(:) = 1;
            % 
        else
            if isempty(positionsMatched)
                % add true positions to dismatch vector
                numDismatch = numDismatch + length(positionsToMatchTo);
                % if no positions to match to, these are false positives
%                  valuesToMatchFalseBinary{k}(:) = 1;
            else
                % loop through the positions of positives
                for j = 1:length(positionsToMatchTo)
                    positionToMatchTo = positionsToMatchTo(j);

                    % when matching, add  positionsMatched+len2 and
                    % positionsMatched-len2 to take care of circular case
                    if sets.circ % find minima positionToMatchTo - positionsMatched, but also allow +-len2 (to cover circular case)
                        % so 49 is distance 1 to 50, but distance to 1 is 2.
                        [minValue,pos] = min(abs(positionToMatchTo-[positionsMatched positionsMatched+len2 positionsMatched-len2]));
                        % what about the case when both have same
                        % positionToMatchTo is always just one element
                        % if there is two minima, always the leftmost will
                        % be chosen. Could have a check running to see if
                        % there is more than one. Need some test for this
                        % function
                        % difference?
%                         [minValue,pos2] = min(minValue);
%                         pos = pos(pos2);
                    else
                        [minValue,pos] = min(abs(positionToMatchTo-positionsMatched));
                    end
                    
                    % we shouldn't match the same position again
                    positionsMatched(mod(pos+1,2)+1) = inf;
                    % at the end there could still be some positions that are
                    % not inf here. These gives some false positives (i.e.
                    % false positives that match to true pixel, but come from
                    % wrong pixel)


                    % now we found the position where this data pixel maps
                    % closest (we usually shouldn't have many to many, just 1-many
                    % and many-1


                    % now check if the values are not farther away that sets.st
                    if minValue <= sets.st
                        % we have a true match
                        numMatch = numMatch + 1;

                        % then should mark which of the valuesToMatch was
                        % mapped to. 
                        % do we care which element is closest? Might be both
                        % the same distance. So this is mapped

                        % note that this is a true match
                        valuesToMatchBinary{k}(j) = 1;
                    else
                        % note that this is false negative
                        numDismatch = numDismatch + 1;
                    end
                end
                valuesToMatchFalseBinary{k}(~isinf(positionsMatched)) = 1;
            end
        end
    end
    
    % true positive rate
    % tp / (tp + fn)
    try
        sensitivity = numMatch./(numMatch+numDismatch);
    catch
        sensitivity = nan;
    end
     % now sensitivity. Total number of elements is the number of elements
     % is  len2 - length(valuesToMatchBinary);
    % elements that 
    try 
        fp = sum(cell2mat(valuesToMatchFalseBinary));
        % true negatives: elements that were not supposed to match anywhere,
        % and were not matched anywhere
        eltsShouldNotBeMatched = cellfun(@(x) isempty(x),trueValuesToMatch);
        eltsNotBeMatched = cellfun(@(x) isempty(x),valuesToMatchFalseBinary);
        tn = sum(eltsNotBeMatched.*eltsShouldNotBeMatched);

        specificity = tn./(tn+fp);
    catch 
        specificity = nan;
    end


end

