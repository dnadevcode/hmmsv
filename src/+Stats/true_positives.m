function [sensitivity] = true_positives(true2,true1,vecb2,vecb1,len2,len1, settingsHMM,sizeM)

    % create valuesToMatch, which store the points matched using the
    % algorithm
    valuesToMatch = cell(1,len1);
    valuesToMatchBinary = cell(1,len1);
%  valuesToMatchFalseBinary = cell(1,len2);
    matchValuesMatchmatrix  = zeros(len1,len2);

    % all values of len1
    allValues = ones(1,len2);
    % stores indices of false postives
%     falsePositiveIdx =zeros(1,len2);

    % query elements might repeat, but data elements assigned are unique.
    % data assigned is in vals2
    % valuesToMatch has as an index query value.
    for k=1:sizeM  
        vals1 = round(vecb1{k});
        vals2 = round(vecb2{k});
        
        % these might not be equal, i.e. [l1,l2] maped to [l1,l2+1]..
        % quickly fix this by cutting of extra pixels
        maxL = min(length(vals2),length(vals1));
        vals2 = vals2(1:maxL);
        vals1 = vals1(1:maxL);
        % create cell with positions that are supposed to be matched
        for jj=1:length(vals1)
            valuesToMatch{vals1(jj)} = [valuesToMatch{vals1(jj)} vals2(jj)];
            valuesToMatchBinary{vals1(jj)} = [valuesToMatchBinary{vals1(jj)} 0];
            allValues(vals2(jj)) = 0;
            matchValuesMatchmatrix(vals1(jj),vals2(jj)) = 1;
        end
    end
    
    valuesToMatchFalseBinary = valuesToMatchBinary;
    trueValuesToMatch = cell(1,len1);
    vals2 = round(true2);
    vals1 = round(true1);        
    for jj=1:length(vals1)
        trueValuesToMatch{vals1(jj)} = [trueValuesToMatch{vals1(jj)} vals2(jj)];
    end
    
    % now go through all trueValuesToMatch and see how they are matched
    numMatch = 0;
    numDismatch = 0;
    for k=1:len1 % length of query
        % where was  supposed to match. Might be multiple.. (say 1 and 40)
        positionsToMatchTo = trueValuesToMatch{k};
        
        % where was matched really
        positionsMatched =  valuesToMatch{k};
        
        if isempty(positionsToMatchTo)
            % case when there are no positives
            % then all positionsMatched are treated as false positives
            valuesToMatchFalseBinary{k}(:) = 1;
            % 
        else
            if isempty(positionsMatched)
                % add true positions to dismatch vector
                numDismatch = numDismatch + length(positionsToMatchTo);
                % if no positions to match to, these are false positives
%                  valuesToMatchFalseBinary{k}(:) = 1;
            else
                % loop through the positions of positives
                for j = 1:length(positionsToMatchTo)
                    positionToMatchTo = positionsToMatchTo(j);

                    % when matching, add  positionsMatched+len2 and
                    % positionsMatched-len2 to take care of circular case
                    if settingsHMM.comparison.circ % find minima positionToMatchTo - positionsMatched, but also allow +-len2 (to cover circular case)
                        [minValue,pos] = min(abs(positionToMatchTo-[positionsMatched positionsMatched+len2 positionsMatched-len2]));
                    else
                        [minValue,pos] = min(abs(positionToMatchTo-positionsMatched));
                    end
                    % we shouldn't match the same position again
                    positionsMatched(mod(pos+1,2)+1) = inf;
                    % now check if the values are not farther away that sets.st
                    if minValue <=  settingsHMM.comparison.st
                        % we have a true match
                        numMatch = numMatch + 1;
                        valuesToMatchBinary{k}(j) = 1;
                    else
                        % note that this is false negative
                        numDismatch = numDismatch + 1;
                    end
                end
                valuesToMatchFalseBinary{k}(~isinf(positionsMatched)) = 1;
            end
        end
    end
    
    % true positive rate
    % tp / (tp + fn)
    try
        sensitivity = numMatch./(numMatch+numDismatch);
    catch
        sensitivity = nan;
    end
     % now sensitivity. Total number of elements is the number of elements
     % is  len2 - length(valuesToMatchBinary);
    % elements that 
    %     try 
    %         fp = sum(cell2mat(valuesToMatchFalseBinary));
    %         % true negatives: elements that were not supposed to match anywhere,
    %         % and were not matched anywhere
    %         eltsShouldNotBeMatched = cellfun(@(x) isempty(x),trueValuesToMatch);
    %         eltsNotBeMatched = cellfun(@(x) isempty(x),valuesToMatchFalseBinary);
    %         tn = sum(eltsNotBeMatched.*eltsShouldNotBeMatched);
    % 
    %         specificity = tn./(tn+fp);
    %     catch 
    %         specificity = nan;
    %     end
end

