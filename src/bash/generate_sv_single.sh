#!/bin/bash

# This runs through all the all_possible files
 args=("$@")
 echo Number of arguments passed: $#
 for var in "$@"
 do
   echo "$var"
 done


cscript=$1
f=${2}
d=${3}

rM=$4
rG=$5
cM=$6
cG=$7
circ=$8

#for f in $fold*data*; do
g=$(sed 's/query/query_output/g' <<< "$f")
echo "$d"
echo "$f"
$cscript $f $d $rM $rG $cM $cG $circ > $g
#done
