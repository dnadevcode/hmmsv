#!/bin/bash

# This runs through all the all_possible files
 args=("$@")
 echo Number of arguments passed: $#
 for var in "$@"
 do
   echo "$var"
 done


cscript=$1
fold=${2}
rM=$3
rG=$4
cM=$5
cG=$6
circ=1

for f in $fold*data*; do
d=$(sed 's/data/query/g' <<< "$f")
g=$(sed 's/query/query_output/g' <<< "$d")
# echo "$d"
# echo "$f"
$cscript $d $f $rM $rG $cM $cG $circ > $g
done
