function [t,matFilepathShort,theoryStruct,theoryGen] = create_cb_barcode(source,bpnm,pixelWidth_nm)
    

%     source = '/home/albyback/git/sv/data/runData/*.fasta';
    import CBT.Hca.Import.import_settings;
    [setsThry] = import_settings('test6/puuhsets.txt');
    setsThry.theoryGen.meanBpExt_nm = bpnm;
    setsThry.theoryGen.pixelWidth_nm = pixelWidth_nm;
%     setsThry.model.pattern = [];
    [t,matFilepathShort,theoryStruct, ~,theoryGen] = HCA_om_theory_parallel(source, bpnm ,setsThry);

    
    
    % create_cb_barcode (some functions from HCA) 
    % start as in validate_G_noise_with_HCA to generate theory

% 
%     % timestamp for the results
%     timestamp = datestr(clock(), 'yyyy-mm-dd_HH_MM_SS');
% 
% 
%     % place where we have data we want to run
% %     source = '/home/albyback/git/sv/data/runData/';
%     addpath(genpath(source));
%     setsF = 'bac_settings.txt';
%     resultFold = 'resultData/';
% 
%     timeFrames = 20;
% 
%     % load tif and lambda names
%     [tifsName,lambdasName] = create_database_files(source,setsF,resultFold);
% 
%     % generate consensus
%     [consensusStructs,consensusStruct, kymoStructs,barcodeGen] = create_consensus( tifsName{1}, setsF, timeFrames );
% 
%     [lambdaValues,lengths,bppx,bpnm,bpnmTheory] = generate_lambda_values( lambdasName{1}, 'bac_settings.txt',10);
%     bpnm = 159.2/bppx;
% 
%     % import settings
%     import CBT.Hca.Import.import_hca_settings;
%     [sets] = import_hca_settings(setsF);
%     % sets.resultsDir = 'out/';
%     % mkdir(sets.resultsDir);
% 
%     % double check puuh_theory.txt
%     % generate theory

    
%     [t,theoriesTxt,theoryStruct, sets,theoryGen] = HCA_theory_parallel(theories,bpnm,'puuh_theory.txt');


end

