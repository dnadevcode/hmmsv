function [newData] = circle_interpolation(vec,len1,len2)
    % Interpolates data on a circle
    
    %   Args:
    %       len1 - max len of barcode
    %       vec -vector with coordinatesto be interpolated
    %       len2 - length to interpolate to
    
    % first coordinate is vec(1). then we want angles to be continuous from
    % this value
    
    % interpolates continuous data on a circle. 
    % first convert data to vector of continuous values
    % convert to angle (between -pi and pi)
    anglecoords = @(x)  (x)/(len1)*360;
    % convert to cartesian
    xpol = @(x) exp(1i.*pi./180 .*anglecoords(x));
    % cartesian to polar
    pos = @(x) (cart2pol(real(x),imag(x))); 
    % values [pi,2pi] has to be converted to [-pi,pi]
    cartesianCoords = xpol(vec);
    
    thresh=anglecoords(vec(1))*pi/180;
    % now conert back    
    polarVals = pos(cartesianCoords);
    polarVals(polarVals<thresh) = polarVals(polarVals<thresh)+2*pi; % values which are [-pi,0] are less than vec(1) are converted
    
    % polar to line
    newValues = @(x) x/(2*pi)*(len1);
    
    % new values
    vecnew = newValues(polarVals);
    
    % now interpolate cartesian coordinates
    newData = round(interp1(1:length(vecnew),vecnew,linspace(1,length(vecnew),len2),'nearest'));
    
    newData = mod(newData-1,len1)+1;
    
end

