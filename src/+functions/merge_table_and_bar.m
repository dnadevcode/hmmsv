function [mergedTable,barsA,barsB,bitA,bitB] = merge_table_and_bar(fT,barfragq,barfragr,bit1,bit2,len1, len2, allowedGap)
    %   Args:
    %
    %   Returns:

    % first: check which ones to merge

    % merge fragments which are less than 5 px apart (1 mean 1 px apart)
    merge = zeros(1,size(fT,1));
    for i=2:size(fT,1)
        if fT(i,5) == fT(i-1,5)
            diff1 = fT(i,2)-fT(i-1,1);
            diff2 = fT(i,4)-fT(i-1,3);
            % if the condition satisfied, merge
            if (abs(diff1)<=allowedGap && abs(diff2)<=allowedGap)...
                    ||  (abs(fT(i,4)-len1-fT(i-1,3))<=allowedGap && abs(fT(i,2)-fT(i-1,1))<=allowedGap)
                merge(i) = 1;
            end
            % condition (abs(mod(fT(i,4)-fT(i-1,3),len2))<=allowedGap:
            % i.e. fT = [50 60 3 13 1 1;1 51 450 500 1 1]
        end
    end
    % creates merge regions
    [labeledA, numRegions] = bwlabel(merge, 4); %0 means start a new region, 1 means merge to new region

% now merge those regions (and together their region barcodes barfragq and
% barfragr)

    prevIdx = 1; % create a merged table
    mergedTable = [];
    barsA = {};
    barsB = {};
    bitA = {};
    bitB = {};
    for i=1:max(labeledA)   
        startS = find(labeledA==i,1);
        endS = find(labeledA==i,1,'last');
        if prevIdx < startS-1
            mergedTable = [mergedTable; fT(prevIdx:startS-2,1:5)];
            barsA = [barsA barfragq{prevIdx:startS-2}];
            barsB = [barsB barfragr{prevIdx:startS-2}];
            bitA = [bitA  bit1{prevIdx:startS-2}];
            bitB = [bitB  bit2{prevIdx:startS-2}];
        end
            %    check that merging does not create double loop. What if
            %    it's a double loop in both?
            % or=1 case:
            overL = 0;
            if fT(endS,5) == 1
                if fT(endS,4) >= fT(startS-1,4) &&  fT(endS,3) <= fT(startS-1,4)
                    overL =  fT(startS-1,4) -  fT(endS,3)+1;
                    mergedTable = [ mergedTable; fT(endS,1) mod(fT(startS-1,2)-overL,length(bit2{1}))...
                    fT(endS ,3) fT(endS ,3)-1 fT(endS,5)];
                else
                    mergedTable = [ mergedTable; fT(endS,1) fT(startS-1,2)...
                    fT(endS ,3) fT(startS-1,4) fT(endS,5)];
                end
            end

            barAnew =  barfragq{endS};
            barBnew = barfragr{endS};
            bitAnew = bit1{endS};
            bitBnew  = bit2{endS};
            for j=endS-1:-1:startS-1
                % I need to check if barsA from consqutive rows are overlapping
                % or not. If this slow, can work with individual positions, but
                % this solution is more user friendly.
                nonNan1 = sum((bit1{endS}.*(bit1{endS-1})));
                nonNan2 = sum(bit2{endS}.*(bit2{endS-1}));
                nonNan = max(nonNan1,nonNan2);
                % if the bitmasks do not overlap, just print both barcodes
                if nonNan == 0
                      barAnew = [barAnew barfragq{j}];
                      barBnew = [barBnew barfragr{j}];
                else
                    barAnew = [barAnew barfragq{j}(nonNan+1:end-overL)];
                    barBnew = [barBnew barfragq{j}(nonNan+1:end-overL)];
                end  
                
                bitAnew(logical(bit1{j}(1:end-overL))) = 1;
                bitBnew(logical(bit2{j}(1:end-overL))) = 1;
            end
        barsA = [barsA barAnew];
        barsB = [barsB barBnew];
        bitA = [bitA  bitAnew];
        bitB = [bitB  bitBnew];
        prevIdx = endS+1;

    end

    if prevIdx <= size(fT,1)
        mergedTable = [mergedTable; fT(prevIdx:end,1:5)];
        barsA = [barsA barfragq{prevIdx:end}];
        barsB = [barsB barfragr{prevIdx:end}];
        bitA = [bitA  bit1{prevIdx:end}];
        bitB = [bitB  bit2{prevIdx:end}];
    end

end

