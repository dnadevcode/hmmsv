function [bar1frag,bar2frag,bitmask1,bitmask2] = table_to_bar(Ttemp, bar1, bar2)
    %   create bar fragments from bar1 and bar2
    %   Args:
    %   Ttemp temporary table
    %   bar1 data
    %   bar2 query
    
    % Returns:
    % bar1frag
    % bar2frag
    %
    
    bitmask2 = zeros(1, length(bar2));
    bitmask1 = zeros(1, length(bar1));

    if Ttemp(1) <= Ttemp(2) && Ttemp(2) <= length(bar2)
        bar2frag = bar2(Ttemp(1):Ttemp(2));
        bitmask2(Ttemp(1):Ttemp(2)) = 1;
    else
        if  Ttemp(2) <= length(bar2)
            bar2frag = [bar2(Ttemp(1):length(bar2)) bar2(1:Ttemp(2))];      
            bitmask2([Ttemp(1):length(bar2) 1:Ttemp(2)] ) = 1;
        else
            bar2frag = [bar2(Ttemp(1):length(bar2)) bar2(1:Ttemp(2)-length(bar2))];      
            bitmask2([Ttemp(1):length(bar2) 1:Ttemp(2)-length(bar2)] ) = 1;
        end
    end

        % barfragq{1} = zeros(1,length(bar2)); 
    if Ttemp(5) == 1
        if Ttemp(end,3) <= Ttemp(end,4)
            bar1frag =  bar1(Ttemp(3):Ttemp(4));
            bitmask1(Ttemp(3):Ttemp(4)) = 1;
        else
            bar1frag =  bar1([Ttemp(3):length(bar1) 1:Ttemp(4)]);
            bitmask1(Ttemp(3):length(bar1)) = 1;
            bitmask1(1:Ttemp(4)) = 1;
        end
        if length(bar1frag)<length(bar2frag) % means they were looping around.. add
            bar1frag = [bar1frag  bar1([Ttemp(4)+1:length(bar1) 1:Ttemp(3)])];
        end
    else
        if Ttemp(end,3) >= Ttemp(end,4)
            bar1frag =  bar1(Ttemp(3):-1:Ttemp(4));
            bitmask1(Ttemp(3):-1:Ttemp(4)) = -1;
        else
            bar1frag =  bar1([Ttemp(3):-1:1 length(bar1):-1:Ttemp(4)]);
            bitmask1([Ttemp(3):-1:1 length(bar1):-1:Ttemp(4)]) = -1;
        end
        if length(bar1frag)<length(bar2frag) % means they were looping around.. add
            bar1frag = [bar1frag  bar1([Ttemp(4)-1:-1:1 length(bar1):-1:Ttemp(3)])];
        end

    end


end

