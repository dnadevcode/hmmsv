function [bitmask1,bitmask2] = table_to_bit(Ttemp, len1, len2)
    %   create bit fragments from bar1 and bar2
    %   Args:
    %           Ttemp temporary table
    %           len1 data length
    %           len2 query length
    
    % Returns:
    % bar1frag
    % bar2frag
    %
    
    bitmask2 = zeros(1,len2);
    bitmask1 = zeros(1, len1);

    if Ttemp(1) < Ttemp(2) && Ttemp(2) <= len2
        bitmask2(Ttemp(1):Ttemp(2)) = 1;
    else
        if  Ttemp(2) <= len2
            bitmask2([Ttemp(1):len2 1:Ttemp(2)] ) = 1;
        else
            bitmask2([Ttemp(1):len2 1:Ttemp(2)-len2] ) = 1;
        end
    end

    if Ttemp(5) == 1
        if Ttemp(end,3) < Ttemp(end,4)
            bitmask1(Ttemp(3):Ttemp(4)) = 1;
        else
            bitmask1(Ttemp(3):len1) = 1;
            bitmask1(1:Ttemp(4)) = 1;

        end
    else
        if Ttemp(end,3) > Ttemp(end,4)
            bitmask1(Ttemp(3):-1:Ttemp(4)) = -1;
        else
            bitmask1([Ttemp(3):-1:1 len1:-1:Ttemp(4)]) = -1;
        end

    end


end

