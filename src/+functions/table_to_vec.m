function [vec,cartesianCoords] = table_to_vec(ab,len1,or1)
    % converts [a,b] values to a,a+1,.ldots,b
    % Args:
    %      ab - start and end coordinates
    %      len1 - total length of barcode
    %      or1 - orientation
    % Returns:
    %      vec - vector with indices in range (1,len1)
    
    % this possibly could be reused when checking if two points are close
    % to each other (when they are at the edges)
    
    if or1 == 1
        if ab(1)<=ab(2)
            vec = ab(1):ab(2);
        else
            vec = [ab(1):len1 1:ab(2)];
        end
    else
        if  ab(1)>ab(2)
            vec = ab(1):-1:ab(2);
        else
            vec = [ab(1):-1:1 1:-1:ab(2)];
        end
    end
        
    % now convert to supported range using angular coordinates
    anglecoords = @(x)  (x)/(len1)*360;
    xpol = @(x) exp(1i.*pi./180 .*anglecoords(x));
    pos = @(x) (cart2pol(real(x),imag(x))); 
    cartesianCoords = xpol(vec);
    polarVals = pos(cartesianCoords);
    polarVals(polarVals<0) = polarVals(polarVals<0)+2*pi; % values which are [-pi,0] are converted to [pi,2pi]
    
    newValues = @(x) x/(2*pi)*(len1);
    vec = newValues(polarVals);


end

