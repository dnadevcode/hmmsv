function [table] = merge_just_table(table, bit1, bit2, len1, len2, allowedGap)
    %   Args:
    %       table, bit1, bit2, len1,len2, allowedGap
    %   Returns:
    %       mergedTable

    % first: check which ones to merge
    
%     if table(end,6) == 1
       maxCluster = table(end,6);
%     else
%         maxCluster
%     end

    % merge fragments which are less than 5 px apart (1 mean 1 px apart)
    merge = zeros(1,size(table,1)); 
    for i=2:size(table,1) % maybe a nicer way to write using bitmasks (and len2?)
        if table(i,5) == table(i-1,5)
            diff1 = table(i,2)-table(i-1,1);
            diff2 = table(i,4)-table(i-1,3);
            % if the condition satisfied, merge
            if (abs(diff1)<=allowedGap && abs(diff2)<=allowedGap)...
                    ||  (abs(table(i,4)-len1-table(i-1,3))<=allowedGap && abs(table(i,2)-table(i-1,1))<=allowedGap)
                merge(i) = 1;
                table(i,6) = table(i-1,6);
            else
%                 if table(i,6)==0
                table(i,6) =  table(i-1,6)+1;
%                 end
            end
            % condition (abs(mod(fT(i,4)-fT(i-1,3),len2))<=allowedGap:
            % i.e. fT = [50 60 3 13 1 1;1 51 450 500 1 1]
        else
            table(i,6) =  table(i-1,6)+1;
        end
    end
    
    if maxCluster==1
        idxToChange =  table(end,6);
        table(table(:,6)==idxToChange,6) = maxCluster;
    end

end

