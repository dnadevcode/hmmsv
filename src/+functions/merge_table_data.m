function [mergedTable] = merge_table_data(T,bar1,bar2,allowedGap,circ)
    % merge data table using the allowed gap. 
    % The merging information is augmented to the table T.
    % And if data was circular, first barcode is merged with last one.
    % Otherwise no merging done. Update version of merge_table - remove
    % merge_table once this works
    
    if nargin < 5
        circ = 1;
    end
    
    if size(T,1) < 1
        mergedTable = T;
%         if size(T,1)==1
%             
%         end
        return;
    end
    % length bar1 (query)
    len1  = length(bar1);
    % length bar2 (data)
    len2 = length(bar2);
    
     
    % check if the value in the table is more than length ( means there's a
    % loop that has to be checked
    dataLoopPixels = T(1,2) - len2;
    
    mergedTable = T;
    mergedTable(:,end) = 0; % initialize indexes to 0
    mergedTable(1,6) = 1;
%     indexFragment = 1;
    
    
    import functions.table_to_bit;
    [bit1, bit2] = arrayfun(@(x) table_to_bit(mergedTable(x,:),len1,len2),1:size(mergedTable,1),'UniformOutput',false);

    % check: could it be that we get a single very long barcode as outcome?
    % then first barcode=last barcode
    import functions.table_to_bit;
    if dataLoopPixels > 0 && T(1,5) == T(end,5)  && size(T,1) > 1 %&& T(1,3)>=T(end,3)
%         if   T(1,5) ==1 &&  T(1,4)
%         [bits1, bits2] = table_to_bit(T(1,:), len1, len2);
%         [bite1, bite2] = table_to_bit(T(end,:), len1, len2);
        
        % find overlap between bitmasks. Also important where the overlap
        % is
        nonNan1 = bit1{1}*bit1{end}';
        nonNan2 = bit2{1}*bit2{end}';
        
        nonoverlap1 = ~bit1{1}*bit1{end}';
        nonoverlap2 = ~bit2{1}*bit2{end}';

        % temp parameters
        nonNan = max(nonNan1,nonNan2);
        difference =  abs(nonNan1-nonNan2);

        if nonNan > 0 && nonoverlap1 > 0 && nonoverlap2 > 0 && difference <= allowedGap %? how to check that this overlap always correct?
%             mergedTable(1,6) = 1;
            mergedTable(end,6) = 1;
        end
    end
    if dataLoopPixels>0
        % remove extra pixels from data.
        if mergedTable(1,5)==1
            mergedTable(1,1:5) = [ mergedTable(1,1) mergedTable(1,2)-dataLoopPixels ...
                mergedTable(1,3) mod(mergedTable(1,4)-dataLoopPixels-1,len1)+1 mergedTable(1,5)];
        else
            % in this case, check if the length is longer than minimal
            % length!
            mergedTable(1,1:5) = [ mergedTable(1,1) mergedTable(1,2)-dataLoopPixels ...
                mergedTable(1,3) mod(mergedTable(1,4)+dataLoopPixels-1,len1)+1 mergedTable(1,5)];
        end
    end
    
    if circ == 0 % check if we can merge first and last in case the barcodes had gaps at left and right
        
        gap1 = mod(find(abs(bit1{end})==1,1,'first')-find(abs(bit1{1})==1,1,'last')-1,len2)+1;
        % hw to properly include?
%         if isempty(gap1) % add the case when we have inversion
%             gap1 = mod(find(bit1{end}==-1,1,'first')-find(bit1{1}==-1,1,'last')-1,len2)+1;
%         end

        gap2 = mod(find(abs(bit2{end})==1,1,'first')-find(abs(bit2{1})==1,1,'last')-1,len1)+1;
        if gap1 <= allowedGap && gap2 <= allowedGap%? how to check that this overlap always correct?
%             mergedTable(1,6) = 1;
            mergedTable(end,6) = 1;
        end

%         if T(1,5) == T(end,5)  && size(T,1) > 1 %&& T(1,3)>=T(end,3)
%             nonNan1 = bit1{1}*bit1{end}';
%             nonNan2 = bit2{1}*bit2{end}';
% 
%             nonoverlap1 = ~bit1{1}*bit1{end}';
%             nonoverlap2 = ~bit2{1}*bit2{end}';
% 
%             % temp parameters
%             nonNan = max(nonNan1,nonNan2);
%             difference =  abs(nonNan1-nonNan2);
% 
%             if nonNan > 0 && nonoverlap1 > 0 && nonoverlap2 > 0 && difference <= allowedGap %? how to check that this overlap always correct?
%     %             mergedTable(1,6) = 1;
%                 mergedTable(end,6) = 1;
%             end
%         end
    end
%     
    import functions.merge_just_table;
    [mergedTable] = merge_just_table(mergedTable, bit1, bit2, len1, len2, allowedGap);

    lens = zeros(1,size(mergedTable,1));
    for i=1:size(mergedTable,1)
        if mergedTable(i,1)<=mergedTable(i,2)
            lens(i) = mergedTable(i,2)-mergedTable(i,1)+1;
        else
           lens(i) = length([mergedTable(i,2):len2 len2:mergedTable(i,1) ]);
        end
    end
    
    % probably should remove all less than specific small length, or less
    % than 22 kbp (since these would be discarded later anyway)
    mergedTable(lens<10,:) = [];
%     
%     import functions.table_to_bit;
%     [bit1, bit2] = arrayfun(@(x) table_to_bit(mergedTable(x,:),len1,len2),1:size(mergedTable,1),'UniformOutput',false);
%     
%     numFragments = max(mergedTable(:,6));
    
%     for i=1:length(numFragments)
%         masks1 = bit1(mergedTable(:,6)==i);
%         masks2 = bit2(mergedTable(:,6)==i);
%         % have to see if there is a double region
%         find
%         figure,imagesc(cell2mat(masks1')')
%         figure,imagesc(cell2mat(masks2')')
% 
%     end
    
%       import functions.merge_table_and_bar;
%     [mergedTable,barsA,barsB,bitA,bitB] = merge_table_and_bar(Ttemp,barfragq,barfragr,bit1,bit2,len1,len2,allowedGap);
% 
    
%      [bitmask1,bitmask2] = table_to_bit(Ttemp, len1, len2)
    
    % this was already calculated in run_hmmsv! (except it also calculated
    % the first one.., so we're kind of duplicating here!
    % create fragment barcodes from the table. OK?
%     import functions.table_to_bar;
%     % [barfragq, barfragr] = table_to_bar(Ttemp(end,:),bar1,bar2);
%     [barfragq, barfragr,bit1,bit2] = arrayfun(@(x) table_to_bar(Ttemp(x,:),bar1,bar2),1:size(Ttemp,1),'UniformOutput',false);

  



%          
%             % Now try merging 1 with end. Note this could create circle
%             % situation! (but this could also happen in other cases, when
%             % query is short and data is long!)
%             [mergedTableM,barsAM,barsBM,bitAM,bitBM] = merge_table_and_bar(mergedTable([end,1],:),barsA([end,1]),barsB([end,1]),bitA([end,1]),bitB([end,1]),len1,len2,allowedGap);
%             if size(mergedTableM,1) == 1
%                 mergedTable(1,:) = mergedTableM;
%                 mergedTable(end,:) = [];
%                 barsA{1} = barsAM{1};barsA(end) = [];
%                 barsB{1} = barsBM{1};barsB(end) = [];
%                 bitA{1} = bitAM{1};bitA(end) = [];
%                 bitB{1} = bitBM{1};bitB(end) = [];
%             end
%            

% 
%     if  dataLoopTrue
%         T(1,2) =T(1,2)-len2;
%         Ttemp = T(2:end,:);
%     else
%         Ttemp = T;
%     end



    % now check the last one vs first one, it's kind of the same except overlap
    % can be longer! (should add this to merge table and bar?


    
end

