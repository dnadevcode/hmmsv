function [finalPos,matchTableOut] = transitive_match_table(matchTable,bar1,bar2)

% in this case lengths are the same
lenA1 = length(bar1{1});
lenA2 =  length(bar2{1});
lenB1 = lenA2;
lenB2 =  length(bar2{2});

% need to create new table of continuously matching values. these are
% circular..
import functions.table_to_vec;
% [vec1,cartesianCoords] = table_to_vec(matchTable{1}(1,1:2),lenA1,1);
% [vec2,cartesianCoords] = table_to_vec(matchTable{1}(1,3:4),lenA2,1);

% first convert table to vector values
[vecb1,~] = arrayfun(@(x) table_to_vec(matchTable{1}(x,1:2),lenA1,1),1:size(matchTable{1},1),'un',false);
[vecb2,~] = arrayfun(@(x) table_to_vec(matchTable{1}(x,3:4),lenA2, matchTable{1}(x,5)),1:size(matchTable{1},1),'un',false);

[vecb3,~] = arrayfun(@(x) table_to_vec(matchTable{2}(x,1:2),lenB1,1),1:size(matchTable{2},1),'un',false);
[vecb4,~] = arrayfun(@(x) table_to_vec(matchTable{2}(x,3:4),lenB2,matchTable{2}(x,5)),1:size(matchTable{2},1),'un',false);

% 1:lenA1
bPos = 1:lenA1;
bPos(round(cell2mat(vecb1))) = round(cell2mat(vecb2));

dPos = 1:lenB1;

dPos(round(cell2mat(vecb3))) = round(cell2mat(vecb4));

finalPos = dPos(bPos);

% find points where it's not 1111 or -1-1-1 - these give us positions of
% the sub-barcodes
diffPos =  diff(finalPos);
subbarcodePos = min((diffPos~=1),(diffPos~=-1));

changePt = find(subbarcodePos);

matchTableOut = zeros(length(changePt),6);

for i=1:length(changePt)-1
   matchTableOut(i,:) = [changePt(i)+1 changePt(i+1) finalPos(changePt(i)+1) finalPos(changePt(i+1)) (diffPos(changePt(i+1)-1)==-1)+1  i];
end

matchTableOut(end,:) = [changePt(end)+1 changePt(1) finalPos(changePt(end)+1) finalPos(changePt(1)) (diffPos(changePt(1)-1)==-1)+1 length(changePt)];
%    findchangepts(finalPos)
% bPos
%    vals = bPos(
   

end

