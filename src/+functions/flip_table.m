function [flippedTable,rescaledBar] = flip_table(table,rescaledBar)
    % flip table so that the looping (circular) element is never inversion
    % (so inversion can happen only in the middle of barcode)

    flippedTable = table;

    if ~isempty(table)
        if table(1,5) == 2
            
            lenBar = length(rescaledBar);
            rescaledBar = fliplr(rescaledBar);


            for i=1:size(table,1)
                if table(i,5) == 2
                        flippedTable(i,3:5) = [lenBar-table(i,3)+1  lenBar-table(i,4)+1 1];
                else
                        flippedTable(i,3:5) = [lenBar-table(i,3)+1  lenBar-table(i,4)+1 2];
                end
            end   
        end
    end

end

