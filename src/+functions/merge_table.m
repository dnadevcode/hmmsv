function [mergedTable,barsA,barsB,bitA,bitB] = merge_table(T,bar1,bar2,allowedGap)
    % merge data table using the allowed gap. 
    % The merging information is augmented to the table T.
    % And if data was circular, first barcode is merged with last one.
    % Otherwise no merging done
    

    len1  = length(bar1);
    len2 = length(bar2);
    
     
    
    dataLoopTrue = T(1,2) > len2;

    if  dataLoopTrue
        T(1,2) =T(1,2)-len2;
        Ttemp = T(2:end,:);
    else
        Ttemp = T;
    end


    % this was already calculated in run_hmmsv! (except it also calculated
    % the first one.., so we're kind of duplicating here!
    % create fragment barcodes from the table. OK?
    import functions.table_to_bar;
    % [barfragq, barfragr] = table_to_bar(Ttemp(end,:),bar1,bar2);
    [barfragq, barfragr,bit1,bit2] = arrayfun(@(x) table_to_bar(Ttemp(x,:),bar1,bar2),1:size(Ttemp,1),'UniformOutput',false);

    import functions.merge_table_and_bar;
    [mergedTable,barsA,barsB,bitA,bitB] = merge_table_and_bar(Ttemp,barfragq,barfragr,bit1,bit2,len1,len2,allowedGap);
% 

    % now check the last one vs first one, it's kind of the same except overlap
    % can be longer! (should add this to merge table and bar?

    if dataLoopTrue
    %     mergedTable = [T(1,1:5); mergedTable];
    %     mergedTable(1,2) =mergedTable(1,2)-len2;
        [barfragq1, barfragr1,bit11,bit21] = table_to_bar(T(1,:),bar1,bar2);

        if T(1,5) == mergedTable(end,5)
            % find overlap
            nonNan1 = sum(abs((bit11.*(bitA{end}))));
            nonNan2 = sum(abs(bit21.*(bitB{end})));
            nonNan = max(nonNan1,nonNan2);
            difference =  abs(nonNan1-nonNan2);

            if nonNan > 0 && abs(nonNan1-nonNan2) <= allowedGap %? how to check that this overlap always correct?
                barsA{end} = [barfragq1(1:end-nonNan+1) barsA{end}]; % a bit strange how I define this here, should be inverted?
                barsB{end} = [barfragr1(1:end-nonNan+1) barsB{end}];
                bitA{end}(logical(bit11)) = 1;
                bitB{end}(logical(bit21)) = 1;
                % substract difference since it's also substracted in barsA
                % and barsB - this way we're guaranteed that data has no
                % double-looping (each pixel indexed once). Should check if
                if mergedTable(end,5)==1
                    mergedTable(end,:) = [ T(1,1) mod(mergedTable(end,2)-difference-1,len1)+1  T(1,3)  mod(mergedTable(end,4)-difference-1,len2)+1 mergedTable(end,5)];
                else
                    mergedTable(end,:) = [ T(1,1) mod(TmergedTable(end,2)-difference,len1)+1  T(1,3)  mod(mergedTable(end,4)+difference-1,len2)+1 mergedTable(end,5)];
                end
                % after this need to make sure that we're within valid
                % entries! otherwise might get bugs..
%                 mergedTable(end,1)
                
                % Now try merging 1 with end. Note this could create circle
                % situation! (but this could also happen in other cases, when
                % query is short and data is long!)
                [mergedTableM,barsAM,barsBM,bitAM,bitBM] = merge_table_and_bar(mergedTable([end,1],:),barsA([end,1]),barsB([end,1]),bitA([end,1]),bitB([end,1]),len1,len2,allowedGap);
                if size(mergedTableM,1) == 1
                    mergedTable(1,:) = mergedTableM;
                    mergedTable(end,:) = [];
                    barsA{1} = barsAM{1};barsA(end) = [];
                    barsB{1} = barsBM{1};barsB(end) = [];
                    bitA{1} = bitAM{1};bitA(end) = [];
                    bitB{1} = bitBM{1};bitB(end) = [];
                end
            end   
        else
            % in the other case, restrict this last barcode to non-overlap?
    %          mergedTable = [T(1,1:5); mergedTable]
        end
        
        
    end
    
end

