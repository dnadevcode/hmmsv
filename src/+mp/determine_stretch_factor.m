function [bestStretch,passthresh,maxCoef,par1,par2,strF] = determine_stretch_factor(query,data,setsComparison,computepval)
    % Use mp or pre-computed bp/nm ratios to determine the best stretch for
    % query vs data

    try
        bestStretch = data.bpnm/query.bpnm;
        passthresh=1;maxCoef=nan,par1=nan;par2=nan;
%         return;
    catch
        bestStretch = nan;
    end

    if ~isnan(bestStretch)
        passthresh = 1;
        return;
    end
    
    % 
    setsComparison.comparison.stretch = [setsComparison.comparison.stretch(1)*length(query.barcode):setsComparison.comparison.stretchPx:setsComparison.comparison.stretch(end)*length(query.barcode)]/length(query.barcode);
    strF = setsComparison.comparison.stretch;
    % needs to separate between linear and circular case ?
    import mp.mp_determine_best_stretch; % here both need to be treated either as circular or linear. 
    % this needs a test function to check that this computes correctly.
    [bestStretch,maxCoef,r] = mp_determine_best_stretch(query.barcode,data.barcode,double(query.bitmask),setsComparison);
    % p-value for best stretch. want to have this pregenerated/ this is a

    
    % heavy p-value generation, where we take len(bar1) vs len(bar2) with
    % window w to generate the p-value distribution
%     sets.lenVar = length(data_txt);

    if computepval % this could be passed for post processing
        % TODO: instead of this use pre-generated p-val database
        if ~isempty(setsComparison.pval.pvalFile)
            import Pvalue.pval_pregenerated;
            % for unique barcodes it's easy. 
            [pvals] = pval_pregenerated(max(maxCoef),r,setsComparison.pval.pvalFile);
            passthresh = pvals < setsComparison.pval.thresh;
        else
            import Core.run_pval_matlab; % we only compute this for best stretch,
            [par1,par2,~,thresh] = run_pval_matlab(round(sum(query.bitmask)*bestStretch),length(data.barcode), r, setsComparison.circ, setsComparison.pval.kernelsigma, setsComparison.pval.numBars);
            passthresh = max(maxCoef)> thresh;
%             p = @(x) 1-(0.5+0.5*(1-betainc((x).^2,0.5,par1/2-1,'upper'))).^par2 ;
%             p(max(maxCoef))
        end
    else
        par1=[];par2=[];
        passthresh = nan;
    end
end

