function [bestStretch,passthresh,maxCoef,par1,par2] = determine_approx_position(query,data,setsComparison,computepval)
    % Use simple sliding pcc with ffts to determine
    % approximate location along the data
    
    % search parameters
    setsComparison.comparison.stretch = [setsComparison.comparison.stretch(1)*length(query.barcode):10:setsComparison.comparison.stretch(end)*length(query.barcode)]/length(query.barcode);
    % needs to separate between linear and circular case
    
    % this now needs to output best location & and stretch!
    import mp.mp_determine_best_stretch; % here both need to be treated either as circular or linear. 
    % this needs a test function to check that this computes correctly.
    [bestStretch,maxCoef,r] = mp_determine_best_stretch(query.barcode,data.barcode,double(query.bitmask),setsComparison);
    % p-value for best stretch. want to have this pregenerated/ this is a

    
    % heavy p-value generation, where we take len(bar1) vs len(bar2) with
    % window w to generate the p-value distribution
%     sets.lenVar = length(data_txt);
    
    % maybe don't compute p-value if the length is long?
    
    if computepval % this could be passed for post processing
        % TODO: instead of this use pre-generated p-val database
        import Core.run_pval_matlab; % we only compute this for best stretch,
        [thresh,par1,par2] = run_pval_matlab(r, round(sum(query.bitmask)*bestStretch),setsComparison);
        passthresh = max(maxCoef)> thresh;
    else
        passthresh = nan;
    end
end

