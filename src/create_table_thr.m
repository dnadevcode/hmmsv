function [resultStruct,bppx] = create_table_thr(trueTable,theoryGenQ,theoryGen,numFragments,bppx)

l2 = length(theoryGenQ.theoryBarcodes{1});
l1 = length(theoryGen.theoryBarcodes{1});

import functions.convert_matchtable;
% outTable =convert_matchtable(res{1});

resultStruct.matchTable = trueTable;
% bppx = max(resultStruct.matchTable(:,4)'); %?

resultStruct.matchTable(:,1:2) = round(resultStruct.matchTable(:,1:2)/(bppx) )+1;
resultStruct.matchTable(:,3:4) = round(resultStruct.matchTable(:,3:4)/(bppx) )+1;
resultStruct.matchTable(:,5) = ones(1,numFragments); 
resultStruct.matchTable(:,6) =ones(1,numFragments);
resultStruct.matchTable = convert_matchtable(resultStruct.matchTable );

resultStruct.bar1 = theoryGenQ.theoryBarcodes{1};
resultStruct.bar2 = theoryGen.theoryBarcodes{1};
resultStruct.pass = [1 1 1 1 1];
end

