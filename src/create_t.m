function [alignmentTable] = create_t(path,S)


    N = 2*S+2;
  % finally path to alignment  table:
    alignmentTable = [];
    
    computingRow=0;
    
    for k= size(path,1):-1:1
        if path(k,2)~=N && path(k,2)~=N-1
            if computingRow==0
                computingRow = 1;
                stRow = k;
            end
        else
            if computingRow~=0  
                if  path(stRow,2) > S
                    alignmentTable = [alignmentTable; path(stRow,1) path(k+1, 1) path(k+1, 2) path(stRow,2)   2];
                else
                    alignmentTable = [alignmentTable; path(stRow,1) path(k+1, 1)  path(stRow,2) path(k+1, 2)  1];
                end
                computingRow = 0;
            end
        end
    end
    
     if computingRow~=0     
        if  path(stRow,2) > S
            alignmentTable = [alignmentTable; path(stRow,1) path(1, 1)   path(1, 2) path(stRow,2)   2];
        else
            alignmentTable = [alignmentTable; path(stRow,1) path(1, 1)   path(stRow,2)  path(1, 2) 1];
        end
        computingRow = 0;
    end
end

