function [par1, par2, ccMax,thresh] = run_pval_matlab(len1, len2, w, circ, kernelsigma,numBars )
    %   Args:
    %       len1 - length query
    %       len2 - lengh data
    %       w - comparison window width
    %       circ - whether barcodes are circular
    %       kernelsigma - sigma for imitating psg
    %       numBars - number of barcodes
    %   Returns:
    %       thresh - when p-value passes thresh
    %       par1 - parameter 1 of EVD fitting
    %       par2 - parameter 2 of EVD fitting

    % this assumes no table was pregenerated
%     if nargin < 3
%         sets.kernelsigma = 300/169.2;
%         sets.lenVar = len2;
%         sets.circ = 1;
%     else
%     %     lenVar = 0
%         try 
%             sets.lenVar;
%         catch
%             sets.lenVar = len2;
%         end
%     end

    svList = zeros(1,numBars);
    % generate sample linear structural variations
%     tic
%     import Rand.generate_linear_sv;
%     [bar1,bar2,matchTable,lengths]  = arrayfun(@(x) generate_linear_sv(len2, sets.lenVar, x,sets),sets.svList,'UniformOutput',false);
%     toc
%     tic  % if circ make sure barcodes are circ
    import Rand.gen_synthetic_sv;
    [bar1, bar2, ~] = gen_synthetic_sv(len1,len2,  svList, circ, kernelsigma, 0);
%     toc
    
    import mp.mp_profile_stomp_dna;
    import Pvalue.compute_evd_params;

    ccMax = zeros(1,length(bar1));
    for idx =1:length(bar1)

        % run this for lengthJ
        if circ % quesion inf both barcodes are circ
            [mp,~] = mp_profile_stomp_dna([bar1{idx} bar1{idx}(1:min(end,w-1))]', [bar2{idx} bar2{idx}(1:w-1)]',w,2^16);
        else
            [mp,~] = mp_profile_stomp_dna([bar1{idx}]', bar2{idx}',w,2^16);
        end 

        ccMax(idx) = max(mp);

    end
    
%     figure,hist(ccMax) // could compute log of this for smaller powers..
%     import Pvalue.compute_evd_params; % x0 should be better bound, make an analysis
    
    params = compute_evd_params(ccMax(:),w/3);
    
    p = @(x) 1-(0.5+0.5*(1-betainc((x).^2,0.5,params(1)/2-1,'upper'))).^params(2) ;
%     we want to have approx region
    thresh = fzero(@(x) p(x)-0.01, [0 1]);
    par1 = params(1);
    par2 = params(2);

end





