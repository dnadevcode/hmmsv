function [match_table,pathV] = hmm_cont(queries,settingsHMM,foldToSaveData,circ)

    % version new:
        par =   { settingsHMM.comparison.cscript,...
            num2str(settingsHMM.comparison.C_M),...
            num2str(settingsHMM.comparison.C_G),...
            num2str(settingsHMM.comparison.c),...
            num2str(settingsHMM.comparison.cg),...
            num2str(circ)};
    
        cmdStr2       = [fullfile(settingsHMM.comparison.bashscript,'generate_sv.sh') ' ' par{1} ' ' fullfile(foldToSaveData,'/') ' ' par{2} ' ' par{3} ' ' par{4} ' ' par{5} ' ' par{6}];
        system(cmdStr2);
        
        import Import.load_hmm_output;
        [match_table,pathV] = cellfun(@(x,y,z) load_hmm_output(x), queries,'un',false);

        for i=1:length(queries)
            [match_table{i},pathV{i}] = load_hmm_output(queries{i});
        end
end

