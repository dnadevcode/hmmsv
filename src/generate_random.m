function [bar1, bar2, matchT, finalPos] = generate_random(sets)


bar1{1} = normrnd(0,1,1,sets.len1);

import Rand.single_sv;
[b2,mT] = arrayfun(@(x) single_sv(bar1{1}, sets.len2, x,  sets.sigma,sets.circ),[sets.typeSv(1)] ,'un',false);
bar2{1} = b2{1};matchTable{1} = mT{1};
[b2,mT] = arrayfun(@(x) single_sv(bar2{1}, sets.len2, x,  sets.sigma,sets.circ),[sets.typeSv(2)] ,'un',false);
bar2{2} = b2{1};matchTable{2}=mT{1};
import Rand.add_psf_sv;
[bar1] = cellfun(@(x) add_psf_sv(x,1, 1.8844),bar1 ,'un',false);
[bar2] = cellfun(@(x) add_psf_sv(x,1, 1.8844),bar2 ,'un',false);

import functions.transitive_match_table;
 [finalPos,matchT] = transitive_match_table(matchTable,bar1,bar2);
 matchTableNew = matchTable;
 matchTableNew{3} = matchT;
import Comparison.pcc;
if sets.noise~= 0
    barRnd = normrnd(0, 1, 1, length(bar2{end}));        
    barRnd =  imgaussfilt(barRnd,  sets.sigma);
    tf = @(x) pcc( bar2{end},(1-x)* bar2{end}+barRnd*x)-(1-sets.noise);
    x0 = 0.5;  x = fzero(tf,x0);
    bar2{end} = (1-x)* bar2{end}+barRnd*x;
end


% sets.svType = { '(A) Inversion', '(B) Translocation', '(C) Combination'};

%  % Fig1-eps-converted-to.pdf
% sets.outfold = 'output';
% import Plot.plot_sv;
% plot_sv( [bar1,bar2{1},bar1], [bar2 bar2{end}],matchTableNew,sets,'Figgg.eps');

end

