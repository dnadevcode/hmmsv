function [t] = determine_noise_dependence_man3(pval1,pM,pG)
    %
    % determine noise dependence used in manuscript v3
    
    tic
    % fix random number generator
    rng(1,'twister');
    
 
%     pvalFile='test/pval_pvalpvalbla2.mat';
    
    
    % timestamp for the results
    timestamp = datestr(clock(), 'yyyy-mm-dd_HH_MM_SS');
    settingsHMM = ini2struct( 'hmmsv_default_settings.txt');
    settingsHMM.timestamp = timestamp;
    mkdir(settingsHMM.output.outfold, settingsHMM.timestamp)
    resFold = fullfile(settingsHMM.output.outfold, settingsHMM.timestamp);
    
%     settingsHMM.comparison.st = 1;
%     settingsHMM.circ = 
%     vals = [0.2:-0.01:0.1];
    vals = [0.25:-0.01:0.05];
    % types of structural variations to consider:
    svTypes = [1,6,2,3,4]; 
%     svTypes = [1]; 


    % first we generate noiseless-data for each of the types

    sets = ini2struct( 'default_noise_simulation_fig5.txt');
    ROCpre = zeros(length(svTypes),length(vals));
    ROCpost = zeros(length(svTypes),length(vals));
    ROCprec = cell(length(svTypes),length(vals));
    ROCpostc = cell(length(svTypes),length(vals));
    % do we want to save the output? Since we use    rng(1,'twister');
    % any error can be regenerated
    if nargin < 3
        [~, pval2] = determine_noise_pval_pregen();
    end
   
    
    % since the lengths are fixed, we'll pregenerate p-value distributions
    % for the 4 cases. insertion and repetition and translocation inversion
    % have same  (500 vs 550). We can compute this for each pixel (40 to
    % 500 ? )
%     settingsHMM.lenVar = 
%     import Core.run_pval_matlab;
%     [thresh1,par1,par2] = run_pval_matlab(x, length(res.bar2),settingsHMM),res.lengths);
    cM = 1/(1-pM)-1;
    cG = 2*sets.len1*(1/(1-pG)-1);

    settingsHMM.comparison.C_M = cM;
    settingsHMM.comparison.C_G = cG;
    settingsHMM.comparison.cscript = 'src/c/test/hmm_console.out';
    sigma = sets.sigma;
    circ = sets.circ;
    c = settingsHMM.comparison.c;
    allowedGap = settingsHMM.comparison.allowedGap;
%     pathToScript = fullfile(settingsHMM.comparison.bashscript,'generate_sv.sh'); 
%         mkdir(resFold2,strcat([num2str(pM) '_' num2str(pG) ]));
%         foldToSaveData = fullfile(resFold2,strcat([num2str(pM) '_' num2str(pG) ])); % final fold where barcodes will be saved

%     par =   {num2str(settingsHMM.comparison.alfabet_size),...
%         num2str(sets.circ),...
%         num2str(settingsHMM.comparison.alfabet_sigma),...
%         num2str(settingsHMM.comparison.C_M),...
%         num2str(settingsHMM.comparison.C_G),...
%         num2str(settingsHMM.comparison.c),...
%         num2str(settingsHMM.comparison.cg),...
%         settingsHMM.comparison.cscript};



    % here we loop over SV types . TODO: put this in another function for
    % better readability
    for idx=1:length(svTypes)
        structVars = svTypes(idx)*ones(1,sets.numBars);
        mkdir(resFold,num2str(svTypes(idx)));
        resFold2= fullfile(resFold,num2str(svTypes(idx)));
        % Generate synthetic data
        import Rand.gen_synthetic_sv;
        [bar1,bar2,matchTable] = gen_synthetic_sv(sets.len1,sets.len2, structVars, sets.circ, sets.sigma,0);
    
        % two types of p-value files pre-generated
        splitName = strsplit(pval1,'_');
        splitName{end-5} = num2str(length(bar2{1}));
        pvalFile = strjoin(splitName,'_');
        % plot - can be removed
            %     idd=37
            %       import Plot.plot_sv;
            % plot_sv({ bar1{idd}}, {bar2{idd}},{matchTable{idd}},sets,'Fig1.eps','df',1);

  
        % here we iterate through different levels of noise
        parfor idy = 1:length(vals)
            noise = vals(idy);
            mkdir(resFold2,num2str(noise));
            foldToSaveData = fullfile(resFold2,num2str(noise)); % final fold where barcodes will be saved

%             import Rand.add_noise; % add noise
            bar2withNoise = cellfun(@(x) Rand.add_noise(x,sigma,noise),bar2,'un',false);
            len1 = cellfun(@length, bar1,'un',false); % barcode lengths
            len2 =  cellfun(@length, bar2withNoise,'un',false);

            % remove any previous data in folder
            system(strcat(['rm ' fullfile(foldToSaveData,'*data*')]));
            system(strcat(['rm ' fullfile(foldToSaveData,'*query*')]));

%             import Import.create_txt_files;
            queries = Import.create_txt_files(foldToSaveData, bar1, bar2withNoise, circ, c);

            [match_table,pathV] = hmm_cont(queries,settingsHMM,foldToSaveData,circ);

            
            [ROCpre(idx,idy),ROCpost(idx,idy)] = match_to_tpr(match_table,matchTable,bar1,bar2withNoise,len1,len2,pvalFile,allowedGap,settingsHMM);
        end
    end
%     for i=1:length(bar1)
%      [passingElts{i}] = mergedTable{i}(ismember(mergedTable{i}(:,6)',find(pccVal{i}<0.01)),:);
%     end

    save(strcat(['noiseoutputs.mat']),'vals','ROCpre','ROCpost', 'ROCpostc', 'ROCprec','svTypes','sets','settingsHMM','-v7.3');

%     save(strcat(['noiseoutputs.mat']),'vals', 'ROCpost', 'ROCpre','-v7.3');

   
    svs= cell(1,length(svTypes));
    for i=1:length(svTypes)
            switch svTypes(i)
                case 0
                    svs{i} = 'Random';
                case 1
                    svs{i} = 'Insertion';
                case 2
                    svs{i}  = 'Inversion';
                case 3
                    svs{i}  = 'Repeat';
                case 4
                    svs{i}  = 'Translocation';
                case 6
                    svs{i}  = 'Deletion';
            otherwise
                error('wrong sv type');
            end
    end

%     markers = ['o';'s','x','+'];
    ColOrd = get(gca, 'ColorOrder');
%     f=figure('Visible', 'on')
    f=figure('Visible', 'on','Position', [10 10 600 400]);
%     f = figure('Position', [10 10 700 650])

    bpPerPx = 500;
    
    hold on
    legendNames = [];
    h = [];
    for i=1:size(ROCpre,1)
        h(i) = plot(1-vals,ROCpre(i,:),'--','color',ColOrd(1+mod(i, 20), :));

        legendNames{i} = strcat([svs{i} ]);
        legendNames{i+5} = strcat([svs{i} ' after $p_{thresh}$']);

%         h(i) = plot(vals,ROCpre(i,:),'color',ColOrd(1+mod(i, 20), :));
        h(i+5) = plot(1-vals,ROCpost(i,:),'-','color',ColOrd(1+mod(i, 20), :));
    end
    title(strcat(['SVs of length ' num2str(sets.len2*bpPerPx/1000) ' kb']),'Interpreter','latex' ) 
    legend(h,legendNames,'Interpreter','latex','Location', 'southeast') %

    xlabel('$dist$','Interpreter','latex');
    ylabel('True positive rate','Interpreter','latex')
    %     legend({'True positive rate after $p_{\rm thresh}$ ','True positive rate before $p_{\rm thresh}$'},'Interpreter','latex','location','best')
    saveas(f,fullfile(settingsHMM.output.figsfold,'Fig4.eps'),'epsc')
    print(f, '-dtiff', fullfile(settingsHMM.output.figsfold ,'/Fig4.tif'));

    t= toc;
    
end


% +4