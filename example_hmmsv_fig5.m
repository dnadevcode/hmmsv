%
% 1) change to the folder with example
cd(fileparts(which('example_hmmsv_fig5.m')));

% 2) generate path
addpath(genpath(pwd));

% 3) create output folder
mkdir("output");
outfold = 'output/';
% 4) compile c code
system("gcc src/c/run_hmm_console.c -o src/c/test/hmm_console.out -lm")

% pre-generated p-value file
pval1 = 'test/pregenerated_500_500_44_500_1.8844_1_pvals.mat';
pval2 = 'test/pregenerated_500_550_44_500_1.8844_1_pvals.mat';

% Example how to run consensus vs consensus for hmmsv. Single query
% and single data

query = repmat({'test/P1K0.mat'},3,1);
data = {'test/P3K0.mat',...
    'test/P5K0.mat',...
    'test/P8K0.mat'};
% ,...
%     '/home/albyback/git/sv/data/mbio_sv/New labels_220kbp all barcodes/P11K22.mat'};
settingsHMM = ini2struct( 'hmmsv_default_settings.txt');

settingsHMM.query.type = 'consensus';
settingsHMM.query.dataFile = 'src/settings/query.txt';

settingsHMM.data.type = 'consensus';
settingsHMM.data.dataFile = 'src/settings/data.txt';

import Import.create_txt_file;
create_txt_file(settingsHMM.query,query);
create_txt_file(settingsHMM.data,data);
settingsHMM.name = 'cons';

settingsHMM.pval.pvalFile = [];

[res0,settingsHMM0] = hmmsv_run(settingsHMM);
    
%%

% Example how to run consensus vs consensus for hmmsv. Single query
% and single data

query = repmat({'test/P11K0.mat'},1,1);
data = {'test/P11K22.mat'};
settingsHMM = ini2struct( 'hmmsv_default_settings.txt');

settingsHMM.query.type = 'consensus';
settingsHMM.query.dataFile = 'src/settings/query.txt';

settingsHMM.data.type = 'consensus';
settingsHMM.data.dataFile = 'src/settings/data.txt';

import Import.create_txt_file;
create_txt_file(settingsHMM.query,query);
create_txt_file(settingsHMM.data,data);
settingsHMM.name = 'cons2';

settingsHMM.pval.pvalFile = [];

[res1,settingsHMM1] =  hmmsv_run(settingsHMM);

%%

query = repmat({'test/P6K0.mat'},1,1);
data = {'test/P6K21.mat'};
settingsHMM = ini2struct( 'hmmsv_default_settings.txt');

settingsHMM.query.type = 'consensus';
settingsHMM.query.dataFile = 'src/settings/query.txt';

settingsHMM.data.type = 'consensus';
settingsHMM.data.dataFile = 'src/settings/data.txt';

import Import.create_txt_file;
create_txt_file(settingsHMM.query,query);
create_txt_file(settingsHMM.data,data);
settingsHMM.name = 'cons3';

settingsHMM.pval.pvalFile = [];

[res2,settingsHMM2] =  hmmsv_run(settingsHMM);

%%
data = repmat({'test/80kb/P6K25.mat'},1,1);
query = {'test/80kb/P6K0.mat'};
settingsHMM = ini2struct( 'hmmsv_default_settings80kb.txt');

settingsHMM.output.figsfold = 'output/';
settingsHMM.query.type = 'consensus';
settingsHMM.query.dataFile = 'src/settings/query.txt';

settingsHMM.data.type = 'consensus';
settingsHMM.data.dataFile = 'src/settings/data.txt';

import Import.create_txt_file;
create_txt_file(settingsHMM.query,query);
create_txt_file(settingsHMM.data,data);
settingsHMM.name = 'cons80kb';

settingsHMM.pval.pvalFile = [];

[res3,settingsHMM3] =  hmmsv_run(settingsHMM)

%% fig 5
%             else
%                 bppx= 215/length(res{j}.bar2);
% f=figure,
f = figure('Position', [10 10 550 850])

% ax = subplot(1, 4,[1 2 3 4]);
ax = subplot(3,1,1)
bppx= settingsHMM3.estlength/length(res3{1}.bar2);
import Plot.plot_sv_fullsim;
plot_sv_fullsim( res3{1},settingsHMM3,strcat(num2str(1),settingsHMM3.name),2,0,bppx);
hold on
ax = subplot(3,1,2)
bppx= 215/length(res0{3}.bar2);

plot_sv_fullsim( res0{3},settingsHMM0,strcat(num2str(1),settingsHMM0.name),2,0,bppx);

ax = subplot(3,1,3)
bppx= 215/length(res2{1}.bar2);

plot_sv_fullsim( res2{1},settingsHMM2,strcat(num2str(1),settingsHMM2.name),2,0,bppx);
 ylabel('Shifted barcode intensities','FontSize', 10,'Interpreter','latex');

saveas(f,fullfile(settingsHMM.output.figsfold,'Fig5.eps'),'epsc')
fullfile(settingsHMM.output.figsfold,strcat(settingsHMM.timestamp,settingsHMM.name))
 print(f, '-dtiff', fullfile(settingsHMM.output.figsfold ,'/Fig5.tif'),'-r300');



