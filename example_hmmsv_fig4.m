%
% 1) change to the folder with example
cd(fileparts(which('example_hmmsv_fig4.m')));

% 2) generate path
addpath(genpath(pwd));

% 3) create output folder
mkdir("output");
outfold = 'output/';
% 4) compile c code
system("gcc src/c/run_hmm_console.c -o src/c/test/hmm_console.out -lm")

% pre-generated p-value file
pval1 = 'test/pregenerated_500_500_44_500_1.8844_1_pvals.mat';
pval2 = 'test/pregenerated_500_550_44_500_1.8844_1_pvals.mat';


pM = 0.51;
pG = 0.31;
% settingsHMM.output.figsfold =  '/home/albyback/git/sv/manuscript/plos-latex-template/figs/';
        
determine_noise_dependence_man3(pval1,pM,pG);