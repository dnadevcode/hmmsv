% FOR GENERATING P-VALUE DATABASES.
rng(1,'twister');

import Pvalue.pregenerate_values;
sets.pval.pvalFile = pregenerate_values(500, 500, 44:500, 1, 1.8844,20,'test2');

rng(1,'twister');
tic
import Pvalue.pregenerate_values;
sets.pval.pvalFile = pregenerate_values(500, 500, 44:500, 1, 1.8844,20,'test3');
toc

rng(1,'twister');
tic
import Pvalue.pregenerate_values_2;
sets.pval.pvalFile = pregenerate_values_2(500, 500, 44:500, 1, 1.8844,1000,'test3');
toc

dat1 = load('test2/pregenerated_500_500_44_500_1.8844_1_pvals.mat');
dat2 = load('test3/pregenerated_500_500_44_500_1.8844_1_pvals.mat');

figure,plot(cell2mat(dat2.par1))
hold on
plot(cell2mat(dat1.par1))